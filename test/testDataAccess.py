#!/usr/bin/env python3
# coding=utf-8
# import
from unittest import TestCase
from random import randint
from dummypythongraphqlserver.entities import Item
from dummypythongraphqlserver.entities import Client
from dummypythongraphqlserver.entities import Address
from dummypythongraphqlserver.entities import PurchaseOrder
from dummypythongraphqlserver.entities import OrderLine
from dummypythongraphqlserver.dataAccess.address import AddressDAO
from dummypythongraphqlserver.dataAccess.item import ItemDAO
from dummypythongraphqlserver.dataAccess.client import ClientDAO
from dummypythongraphqlserver.dataAccess.purchaseOrder import PurchaseOrderDAO
from dummypythongraphqlserver.dataAccess.orderLine import OrderLineDAO
# test class
class testDataAccess(TestCase):
    def testItem(self):
        # clean
        ItemDAO.delete(name="ITEM#", price=1)
        # create
        originalItems = [
            Item("item#0", 1.0),
            Item("item#1", 1.1)
        ]
        newItems = ItemDAO.create(originalItems)
        ids=list()
        for newItem in newItems :
            self.assertIsNotNone(newItem.id)
            ids.append(newItem.id)
        # read
        readItems = ItemDAO.read(ids=ids, name="ITEM#", price=1, limit=len(ids))
        self.assertListEqual(readItems,newItems)
        # update
        newItems = [
            Item("item#2", 1.2, ids[0]),
            Item("item#3", 1.3, ids[1])
        ]
        ItemDAO.update(newItems)
        readItems = ItemDAO.read(ids=ids, name="ITEM#", price=1)
        self.assertListEqual(readItems,newItems)
        # delete
        ItemDAO.delete(ids,"ITEM#", 1)
        readItems = ItemDAO.read(ids=ids, name="ITEM#", price=1)
        self.assertEqual(len(readItems),0)
        pass
    def testAddress(self):
        # clean
        AddressDAO.delete(houseNumber=1, street="STREET#", zipCode=10, city="CITY#")
        # create
        originalAddresses = [
            Address(10, "street#0", 100, "city#0"),
            Address(11, "street#1", 101, "city#1")
        ]
        newAddresses = AddressDAO.create(originalAddresses)
        ids=list()
        for newAddress in newAddresses :
            self.assertIsNotNone(newAddress.id)
            ids.append(newAddress.id)
        # read
        readAddresses = AddressDAO.read(ids=ids, houseNumber=1, street="STREET#", zipCode=10, city="CITY#", limit=len(ids))
        self.assertListEqual(readAddresses,newAddresses)
        # update
        newAddresses = [
            Address(12, "street#2", 102, "city#2", ids[0]),
            Address(13, "street#3", 103, "city#3", ids[1])
        ]
        AddressDAO.update(newAddresses)
        readAddresses = AddressDAO.read(ids=ids, houseNumber=1, street="STREET#", zipCode=10, city="CITY#")
        self.assertListEqual(readAddresses,newAddresses)
        # delete
        AddressDAO.delete(ids,1, "STREET#", 10, "CITY#")
        readAddresses = AddressDAO.read(ids=ids, houseNumber=1, street="STREET#", zipCode=10, city="CITY#")
        self.assertEqual(len(readAddresses),0)
        pass
    def testClient(self):
        # clean
        ClientDAO.delete(firstName="FIRST_NAME#", lastName="LAST_NAME#")
        # create
        readAddresses = AddressDAO.read(range(1,5))
        originalClients = {
            Client("first_name#0", "last_name#0"): readAddresses[0].id,
            Client("first_name#1", "last_name#1"): readAddresses[1].id
        }
        newClients = ClientDAO.create(originalClients)
        ids=list()
        for newClient in newClients :
            self.assertIsNotNone(newClient.id)
            ids.append(newClient.id)
        # read
        readClients = ClientDAO.read(ids=ids, firstName="FIRST_NAME#" ,lastName="LAST_NAME#",limit=len(ids))
        self.assertListEqual(readClients,newClients)
        # update
        newClients = {
            Client("first_name#2", "last_name#2", ids[0]): readAddresses[2].id,
            Client("first_name#3", "last_name#3", ids[1]): readAddresses[3].id
        }
        ClientDAO.update(newClients)
        readClients = ClientDAO.read(ids=ids, firstName="FIRST_NAME#" ,lastName="LAST_NAME#")
        self.assertListEqual(readClients,list(newClients.keys()))
        # delete
        ClientDAO.delete(ids, "FIRST_NAME#", "LAST_NAME#")
        readClients = ClientDAO.read(ids=ids, firstName="FIRST_NAME#" ,lastName="LAST_NAME#")
        self.assertEqual(len(readClients),0)
        pass
    def testPurchaseOrderLine(self):
        # clean
        PurchaseOrderDAO.delete(minimumCreationDate=20200101, maximumCreationDate=20201231)
        # create purchase orders
        clients = ClientDAO.read(limit=2)
        originalPurchaseOrders = {
            PurchaseOrder(20200101): clients[0].id,
            PurchaseOrder(20201231): clients[1].id
        }
        newPurchaseOrders = PurchaseOrderDAO.create(originalPurchaseOrders)
        purchaseOrdersIds=list()
        for newPurchaseOrder in newPurchaseOrders :
            self.assertIsNotNone(newPurchaseOrder.id)
            purchaseOrdersIds.append(newPurchaseOrder.id)
        # create order lines
        itemsSplit = 3
        items = ItemDAO.read(limit=2*itemsSplit)
        originalOrderLines = dict()
        for item in items:
            orderLine = OrderLine(quantity=randint(1,100))
            originalOrderLines[orderLine] = [purchaseOrdersIds[0],item.id]
        newOrderLines = OrderLineDAO.create(originalOrderLines)
        orderLinesIds=list()
        for newOrderLine in newOrderLines :
            self.assertIsNotNone(newOrderLine.id)
            orderLinesIds.append(newOrderLine.id)
        # read purchase orders
        readPurchaseOrders = PurchaseOrderDAO.read(ids=purchaseOrdersIds, minimumCreationDate=20200101, maximumCreationDate=20201231, limit=len(purchaseOrdersIds))
        self.assertListEqual(readPurchaseOrders,newPurchaseOrders)
        # read order lines
        readOrderLines = OrderLineDAO.read(ordersIds=purchaseOrdersIds)
        self.assertListEqual(readOrderLines,newOrderLines)
        # update
        clients = ClientDAO.read(limit=2, offset=2)
        newPurchaseOrders = {
            PurchaseOrder(20200202, purchaseOrdersIds[0]): clients[0].id,
            PurchaseOrder(20201130, purchaseOrdersIds[1]): clients[1].id
        }
        PurchaseOrderDAO.update(newPurchaseOrders)
        readPurchaseOrders = PurchaseOrderDAO.read(ids=purchaseOrdersIds, minimumCreationDate=20200101, maximumCreationDate=20201231)
        self.assertListEqual(readPurchaseOrders,list(newPurchaseOrders.keys()))
        # delete
        PurchaseOrderDAO.delete(purchaseOrdersIds,20200101,20201231)
        readPurchaseOrders = PurchaseOrderDAO.read(ids=purchaseOrdersIds, minimumCreationDate=20200101, maximumCreationDate=20201231)
        self.assertEqual(len(readPurchaseOrders),0)
        readOrderLines = OrderLineDAO.read(ordersIds=purchaseOrdersIds)
        self.assertEqual(len(readOrderLines),0)
        pass
    pass
pass