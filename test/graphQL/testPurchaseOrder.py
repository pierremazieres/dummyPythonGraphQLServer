#!/usr/bin/env python3
# coding=utf-8
# import
from random import randint
from unittest import TestCase
from test.graphQL import client
from time import sleep
from threading import Thread
from dummypythongraphqlserver.graphQL import schema, SubscriptionObserver
from dummypythongraphqlserver.entities import PurchaseOrder
# test class
class testPurchaseOrder(TestCase):
    dynamicQuery = """query ($readPurchaseOrdersInput:ReadPurchaseOrdersInput!,$withOrderLines:Boolean!,$withClient:Boolean!,$withAddress:Boolean!) {
        readPurchaseOrders (readPurchaseOrdersInput:$readPurchaseOrdersInput,withOrderLines:$withOrderLines,withClient:$withClient,withAddress:$withAddress) {
            id,
            creationDate,
            orderLines @include(if: $withOrderLines) {
                item {
                    name,
                    price
                },
                quantity
            },
            client @include(if: $withClient) {
                firstName,
                lastName,
                address @include(if: $withAddress) {
                    houseNumber,
                    street,
                    zipCode,
                    city
                }
            }
        }
    }"""
    def testPurchaseOrder00(self):
        query = """{
            readPurchaseOrders (readPurchaseOrdersInput:{minimumCreationDate:20150125}) {
                id,
                creationDate
            }
        }"""
        executed = client.execute(query)
        expected = {'readPurchaseOrders': [{'id': '11', 'creationDate': 20150125}, {'id': '13', 'creationDate': 20151210}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testPurchaseOrder01(self):
        query = """{
            readPurchaseOrders (readPurchaseOrdersInput:{minimumCreationDate:20120622,maximumCreationDate:20150125}) {
                creationDate
            }
        }"""
        executed = client.execute(query)
        expected = {'readPurchaseOrders': [{'creationDate': 20121227}, {'creationDate': 20120622}, {'creationDate': 20150125}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testPurchaseOrder02(self):
        query = """{
            readPurchaseOrders (readPurchaseOrdersInput:{limit:3,offset:2}) {
                id
            }
        }"""
        executed = client.execute(query)
        expected = {'readPurchaseOrders': [{'id': '3'}, {'id': '4'}, {'id': '5'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testPurchaseOrder03(self):
        variables = {"readPurchaseOrdersInput": {"minimumCreationDate":20120622, "maximumCreationDate":20150125}, "withOrderLines": True, "withClient": True, "withAddress": True}
        executed = client.execute(testPurchaseOrder.dynamicQuery,variables=variables)
        expected = {'readPurchaseOrders': [{'id': '7', 'creationDate': 20121227, 'orderLines': [{'item': {'name': 'wheat', 'price': 2.1}, 'quantity': 64}, {'item': {'name': 'brown mushroom', 'price': 6.3}, 'quantity': 75}], 'client': {'firstName': 'Frank', 'lastName': 'Zappa', 'address': {'houseNumber': '3', 'street': 'Jockey Hollow Dr.', 'zipCode': '6606', 'city': 'Bridgeport'}}}, {'id': '10', 'creationDate': 20120622, 'orderLines': [{'item': {'name': 'melon (block)', 'price': 25.15}, 'quantity': 53}, {'item': {'name': 'beef/steak (raw)', 'price': 1.05}, 'quantity': 85}, {'item': {'name': 'cookie', 'price': 1.6}, 'quantity': 65}, {'item': {'name': 'fish (raw)', 'price': 20.9}, 'quantity': 54}], 'client': {'firstName': 'Anne', 'lastName': 'Hathaway', 'address': {'houseNumber': '3', 'street': 'Jockey Hollow Dr.', 'zipCode': '6606', 'city': 'Bridgeport'}}}, {'id': '11', 'creationDate': 20150125, 'orderLines': [], 'client': {'firstName': 'Rush', 'lastName': 'Limbaugh', 'address': {'houseNumber': '935', 'street': 'Pineknoll Street', 'zipCode': '18951', 'city': 'Quakertown'}}}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testPurchaseOrder04(self):
        variables = {"readPurchaseOrdersInput": {"minimumCreationDate":20120622, "maximumCreationDate":20150125}, "withOrderLines": True, "withClient": False, "withAddress": True}
        executed = client.execute(testPurchaseOrder.dynamicQuery,variables=variables)
        expected = {'readPurchaseOrders': [{'id': '7', 'creationDate': 20121227, 'orderLines': [{'item': {'name': 'wheat', 'price': 2.1}, 'quantity': 64}, {'item': {'name': 'brown mushroom', 'price': 6.3}, 'quantity': 75}]}, {'id': '10', 'creationDate': 20120622, 'orderLines': [{'item': {'name': 'melon (block)', 'price': 25.15}, 'quantity': 53}, {'item': {'name': 'beef/steak (raw)', 'price': 1.05}, 'quantity': 85}, {'item': {'name': 'cookie', 'price': 1.6}, 'quantity': 65}, {'item': {'name': 'fish (raw)', 'price': 20.9}, 'quantity': 54}]}, {'id': '11', 'creationDate': 20150125, 'orderLines': []}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testPurchaseOrder05(self):
        variables = {"readPurchaseOrdersInput": {"minimumCreationDate":20150125}, "withOrderLines": False, "withClient": False, "withAddress": False}
        executed = client.execute(testPurchaseOrder.dynamicQuery,variables=variables)
        expected = {'readPurchaseOrders': [{'id': '11', 'creationDate': 20150125}, {'id': '13', 'creationDate': 20151210}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testPurchaseOrder06(self):
        variables = {"readPurchaseOrdersInput": {"minimumCreationDate":20150125}, "withOrderLines": False, "withClient": True, "withAddress":False }
        executed = client.execute(testPurchaseOrder.dynamicQuery,variables=variables)
        expected = {'readPurchaseOrders': [{'id': '11', 'creationDate': 20150125, 'client': {'firstName': 'Rush', 'lastName': 'Limbaugh'}}, {'id': '13', 'creationDate': 20151210, 'client': {'firstName': 'Toby', 'lastName': 'Keith'}}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testPurchaseOrder07(self):
        query = """{
            readPurchaseOrders (readPurchaseOrdersInput:{minimumCreationDate:"20150125"}) {
                id
            }
        }"""
        executed = client.execute(query)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "readPurchaseOrdersInput" has invalid value {minimumCreationDate: "20150125"}.\nIn field "minimumCreationDate": Expected type "Int", found "20150125".""")
        pass
    def testPurchaseOrder08(self):
        query = """{
            readPurchaseOrders (purchaseOrdersInput_:{minimumCreationDate:20150125}) {
                id
            }
        }"""
        executed = client.execute(query)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "purchaseOrdersInput_" on field "readPurchaseOrders" of type "Query". Did you mean "readPurchaseOrdersInput"?""")
        pass
    def testPurchaseOrder09(self):
        # clean
        deleteMutation = """mutation {
            deletePurchaseOrders(deletePurchaseOrdersInput:{minimumCreationDate:20200101, maximumCreationDate:20201231}) {confirmation}
        }"""
        client.execute(deleteMutation)
        # create
        mutation = """mutation {
            createPurchaseOrders (createPurchaseOrdersInput:[
                {
                    creationDate:20200101,
                    clientId:1,
                    orderLines:[
                        {
                            itemId:1,
                            quantity:"""+str(randint(1,100))+"""
                        },
                        {
                            itemId:2,
                            quantity:"""+str(randint(1,100))+"""
                        }
                    ]
                },
                {
                    creationDate:20201231,
                    clientId:2,
                    orderLines:[
                        {
                            itemId:3,
                            quantity:"""+str(randint(1,100))+"""
                        },
                        {
                            itemId:4,
                            quantity:"""+str(randint(1,100))+"""
                        }
                    ]
                }
            ]) {
                readPurchaseOrders
                {
                    id,
                    orderLines {id}
                }
            }
        }"""
        executed = client.execute(mutation)
        ordersIds=list()
        orderLinesIds=list()
        for purchaseOrder in executed["data"]["createPurchaseOrders"]["readPurchaseOrders"] :
            self.assertIsNotNone(purchaseOrder["id"])
            ordersIds.append(purchaseOrder["id"])
            for orderLine in purchaseOrder["orderLines"] :
                self.assertIsNotNone(orderLine["id"])
                orderLinesIds.append(orderLine["id"])
        # update
        mutation = """mutation {
            updatePurchaseOrders (updatePurchaseOrdersInput:[
                {
                    id:"""+ordersIds[0]+""",
                    creationDate:20200102,
                    clientId:3,
                    orderLines:[
                        {
                            id:"""+orderLinesIds[0]+""",
                            itemId:5,
                            quantity:"""+str(randint(1,100))+"""
                        },
                        {
                            id:"""+orderLinesIds[1]+""",
                            itemId:6,
                            quantity:"""+str(randint(1,100))+"""
                        }
                    ]
                },
                {
                    id:"""+ordersIds[1]+""",
                    creationDate:20201230,
                    clientId:4,
                    orderLines:[
                        {
                            id:"""+orderLinesIds[2]+""",
                            itemId:7,
                            quantity:"""+str(randint(1,100))+"""
                        },
                        {
                            id:"""+orderLinesIds[3]+""",
                            itemId:8,
                            quantity:"""+str(randint(1,100))+"""
                        }
                    ]
                }
            ]) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertTrue(executed["data"]["updatePurchaseOrders"]["confirmation"])
        # delete
        executed = client.execute(deleteMutation)
        self.assertTrue(executed["data"]["deletePurchaseOrders"]["confirmation"])
        pass
    def testPurchaseOrder10(self):
        # clean
        deleteMutation = """mutation deleteMutation ($deletePurchaseOrdersInput:DeletePurchaseOrdersInput!) {
            deletePurchaseOrders(deletePurchaseOrdersInput:$deletePurchaseOrdersInput) {confirmation}
        }"""
        deleteVariables = {"deletePurchaseOrdersInput": {"minimumCreationDate":20200101, "maximumCreationDate":20201231}}
        client.execute(deleteMutation,variables=deleteVariables)
        # create
        mutation = """mutation createMutation ($createPurchaseOrdersInput:[CreatePurchaseOrdersInput!]!, $withClient:Boolean!, $withAddress:Boolean!, $withOrderLines:Boolean!) {
            createPurchaseOrders (createPurchaseOrdersInput:$createPurchaseOrdersInput, withClient:$withClient, withAddress:$withAddress, withOrderLines:$withOrderLines) {
                readPurchaseOrders {
                    id,
                    client @include(if: $withClient) {
                        id,
                        address @include(if: $withAddress) {id}
                    },
                    orderLines @include(if: $withOrderLines) {id}
               }
            }
        }"""
        variables = {"createPurchaseOrdersInput": [
                {
                    "creationDate":20200101,
                    "clientId":1,
                    "orderLines":[
                        {
                            "itemId":1,
                            "quantity":randint(1,100)
                        },
                        {
                            "itemId":2,
                            "quantity":randint(1,100)
                        }
                    ]
                },
                {
                    "creationDate":20201231,
                    "clientId":2,
                    "orderLines":[
                        {
                            "itemId":3,
                            "quantity":randint(1,100)
                        },
                        {
                            "itemId":4,
                            "quantity":randint(1,100)
                        }
                    ]
                }
            ], "withClient": True, "withAddress": True, "withOrderLines": True}
        executed = client.execute(mutation,variables=variables)
        ordersIds=list()
        orderLinesIds=list()
        for purchaseOrder in executed["data"]["createPurchaseOrders"]["readPurchaseOrders"] :
            self.assertIsNotNone(purchaseOrder["id"])
            self.assertIsNotNone(purchaseOrder["client"])
            self.assertIsNotNone(purchaseOrder["client"]["address"])
            self.assertIsNotNone(purchaseOrder["orderLines"])
            ordersIds.append(purchaseOrder["id"])
            for orderLine in purchaseOrder["orderLines"]:
                orderLinesIds.append(orderLine["id"])
        # update
        mutation = """mutation updateMutation ($updatePurchaseOrdersInput:[UpdatePurchaseOrdersInput!]!) {
            updatePurchaseOrders (updatePurchaseOrdersInput:$updatePurchaseOrdersInput) {confirmation}
        }"""
        variables = {"updatePurchaseOrdersInput": [
                {
                    "id":ordersIds[0],
                    "creationDate":20200102,
                    "clientId":3,
                    "orderLines":[
                        {
                            "id": orderLinesIds[0],
                            "itemId":5,
                            "quantity":randint(1,100)
                        },
                        {
                            "id": orderLinesIds[1],
                            "itemId":6,
                            "quantity":randint(1,100)
                        }
                    ]
                },
                {
                    "id":ordersIds[1],
                    "creationDate":20201230,
                    "clientId":4,
                    "orderLines":[
                        {
                            "id": orderLinesIds[2],
                            "itemId":7,
                            "quantity":randint(1,100)
                        },
                        {
                            "id": orderLinesIds[3],
                            "itemId":8,
                            "quantity":randint(1,100)
                        }
                    ]
                }
            ]}
        executed = client.execute(mutation,variables=variables)
        self.assertTrue(executed["data"]["updatePurchaseOrders"]["confirmation"])
        # delete
        executed = client.execute(deleteMutation,variables=deleteVariables)
        self.assertTrue(executed["data"]["deletePurchaseOrders"]["confirmation"])
        pass
    def testPurchaseOrder11(self):
        mutation = """mutation {
            deletePurchaseOrders(deletePurchaseOrdersInput:{minimumCreationDate:"a", maximumCreationDate:20201231}) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "deletePurchaseOrdersInput" has invalid value {minimumCreationDate: "a", maximumCreationDate: 20201231}.\nIn field "minimumCreationDate": Expected type "Int", found "a".""")
        pass
    def testPurchaseOrder12(self):
        mutation = """mutation {
            deletePurchaseOrders(deletePurchaseOrdersInput_:{minimumCreationDate:20200101, maximumCreationDate:20201231}) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "deletePurchaseOrdersInput_" on field "deletePurchaseOrders" of type "Mutation". Did you mean "deletePurchaseOrdersInput"?""")
        pass
    def testPurchaseOrder13(self):
        mutation = """mutation {
            createPurchaseOrders(createPurchaseOrdersInput:[{creationDate:"a",clientId:1,orderLines:[{itemId:1,quantity:1}]}]) {PurchaseOrders {id}}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "createPurchaseOrdersInput" has invalid value [{creationDate: "a", clientId: 1, orderLines: [{itemId: 1, quantity: 1}]}].\nIn element #0: In field "creationDate": Expected type "Int", found "a".""")
        pass
    def testPurchaseOrder14(self):
        mutation = """mutation {
            createPurchaseOrders(createPurchaseOrdersInput_:[{creationDate:20200101,clientId:1,orderLines:[{itemId:1,quantity:1}]}]) {PurchaseOrders {id}}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "createPurchaseOrdersInput_" on field "createPurchaseOrders" of type "Mutation". Did you mean "createPurchaseOrdersInput"?""")
        pass
    pass
    def testPurchaseOrder15(self):
        mutation = """mutation {
            updatePurchaseOrders(updatePurchaseOrdersInput:[{id: 1, creationDate:"a",clientId:1,orderLines:[{id: 1, itemId:1,quantity:1}]}]) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "updatePurchaseOrdersInput" has invalid value [{id: 1, creationDate: "a", clientId: 1, orderLines: [{id: 1, itemId: 1, quantity: 1}]}].\nIn element #0: In field "creationDate": Expected type "Int", found "a".""")
        pass
    def testPurchaseOrder16(self):
        mutation = """mutation {
            updatePurchaseOrders(updatePurchaseOrdersInput_:[{id: 1, creationDate:20200101,clientId:1,orderLines:[{id: 1, itemId:1,quantity:1}]}]) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "updatePurchaseOrdersInput_" on field "updatePurchaseOrders" of type "Mutation". Did you mean "updatePurchaseOrdersInput"?""")
        pass
    pass
    def testPurchaseOrder17(self):
        # subscribe
        subscription = """subscription ($readPurchaseOrdersInput:ReadPurchaseOrdersInput!,$period:Int!,$occurencesNumber:Int!,$withOrderLines:Boolean!,$withClient:Boolean!,$withAddress:Boolean!) {
            readPurchaseOrders (readPurchaseOrdersInput:$readPurchaseOrdersInput,period:$period,occurencesNumber:$occurencesNumber,withOrderLines:$withOrderLines,withClient:$withClient,withAddress:$withAddress) {
                id,
                creationDate,
                orderLines @include(if: $withOrderLines) {
                    item {
                        name,
                        price
                    },
                    quantity
                },
                client @include(if: $withClient) {
                    firstName,
                    lastName,
                    address @include(if: $withAddress) {
                        houseNumber,
                        street,
                        zipCode,
                        city
                    }
                }
            }
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        variables = {"readPurchaseOrdersInput": {"minimumCreationDate":20200101, "maximumCreationDate":20201231}, "period":1, "occurencesNumber":20, "withOrderLines": True, "withClient": True, "withAddress": True}
        # run observer & mutation thread
        observer = TestPurchaseOrdersObserver(subscription, variables)
        TestPurchaseOrdersUpdateThread(int(observer.period*observer.occurencesNumber/4)).start()
        observable = schema.execute(subscription,variables=variables,allow_subscriptions=True)
        # INFO : timestamp decorator function increment a loop conter name 'value'
        observable.interval(observer.period*1000).timestamp().filter(lambda self : self.value < observer.occurencesNumber).subscribe(observer=observer)
        sleep(observer.period*(observer.occurencesNumber+1))
        # check results
        self.assertEqual(len(observer.results), 3)
        self.assertIn(tuple(), observer.results)
        pass
    def testPurchaseOrder18(self):
        # subscribe
        subscription = """subscription ($readPurchaseOrdersInput:ReadPurchaseOrdersInput!,$period:Int!,$occurencesNumber:Int!,$withOrderLines:Boolean!,$withClient:Boolean!,$withAddress:Boolean!) {
            readPurchaseOrders (readPurchaseOrdersInput:$readPurchaseOrdersInput,period:$period,occurencesNumber:$occurencesNumber,withOrderLines:$withOrderLines,withClient:$withClient,withAddress:$withAddress) {id}
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        variables = {"readPurchaseOrdersInput": {"minimumCreationDate":"a", "maximumCreationDate":20201231}, "period":1, "occurencesNumber":20, "withOrderLines": True, "withClient": True, "withAddress": True}
        # run observable
        observable = schema.execute(subscription,variables=variables,allow_subscriptions=True)
        # check results
        self.assertEqual(str(observable.errors[0]),"""could not convert string to float: 'a'""")
        pass
    pass
# test subscription
class TestPurchaseOrdersObserver(SubscriptionObserver):
    # methods
    @staticmethod
    def instanciate(result):
        instance = PurchaseOrder(creationDate=result.get("creationDate") ,id=result.get("id"))
        return instance
    # inheritance
    def on_next(self, value):
        result = super().on_next(value)
        instances = list()
        for element in result.data["readPurchaseOrders"]:
            instance = TestPurchaseOrdersObserver.instanciate(element)
            instances.append(instance)
            pass
        self.results.add(tuple(instances))
        #print("DEBUG :PurchaseOrdersObserver.on_next")
    # constructor
    def __init__(self, subscription,variables):
        super().__init__(subscription,variables)
        self.results = set()
    pass
class TestPurchaseOrdersUpdateThread(Thread):
    def run(self):
        # clean
        deleteMutation = """mutation {
            deletePurchaseOrders(deletePurchaseOrdersInput:{minimumCreationDate:20200101, maximumCreationDate:20201231}) {confirmation}
        }"""
        client.execute(deleteMutation)
        #print("DEBUG :CLEAN")
        sleep(self.waitTime)
        # create
        mutation = """mutation createMutation ($createPurchaseOrdersInput:[CreatePurchaseOrdersInput!]!) {
            createPurchaseOrders (createPurchaseOrdersInput:$createPurchaseOrdersInput) {
                readPurchaseOrders {
                    id,
                    orderLines{id}
                }
            }
        }"""
        variables = {"createPurchaseOrdersInput": [
                {
                    "creationDate":20200101,
                    "clientId":1,
                    "orderLines":[
                        {
                            "itemId":1,
                            "quantity":randint(1,100)
                        },
                        {
                            "itemId":2,
                            "quantity":randint(1,100)
                        }
                    ]
                },
                {
                    "creationDate":20201231,
                    "clientId":2,
                    "orderLines":[
                        {
                            "itemId":3,
                            "quantity":randint(1,100)
                        },
                        {
                            "itemId":4,
                            "quantity":randint(1,100)
                        }
                    ]
                }
            ], "withClient": True, "withPurchaseOrder": True, "withOrderLines": True}
        executed = client.execute(mutation,variables=variables)
        ordersIds=list()
        orderLinesIds=list()
        for purchaseOrder in executed["data"]["createPurchaseOrders"]["readPurchaseOrders"] :
            ordersIds.append(purchaseOrder["id"])
            for orderLine in purchaseOrder["orderLines"]:
                orderLinesIds.append(orderLine["id"])
        #print("DEBUG :INSERT")
        sleep(self.waitTime)
        # update
        mutation = """mutation updateMutation ($updatePurchaseOrdersInput:[UpdatePurchaseOrdersInput!]!) {
            updatePurchaseOrders (updatePurchaseOrdersInput:$updatePurchaseOrdersInput) {confirmation}
        }"""
        variables = {"updatePurchaseOrdersInput": [
                {
                    "id":ordersIds[0],
                    "creationDate":20200102,
                    "clientId":3,
                    "orderLines":[
                        {
                            "id": orderLinesIds[0],
                            "itemId":5,
                            "quantity":randint(1,100)
                        },
                        {
                            "id": orderLinesIds[1],
                            "itemId":6,
                            "quantity":randint(1,100)
                        }
                    ]
                },
                {
                    "id":ordersIds[1],
                    "creationDate":20201230,
                    "clientId":4,
                    "orderLines":[
                        {
                            "id": orderLinesIds[2],
                            "itemId":7,
                            "quantity":randint(1,100)
                        },
                        {
                            "id": orderLinesIds[3],
                            "itemId":8,
                            "quantity":randint(1,100)
                        }
                    ]
                }
            ]}
        client.execute(mutation,variables=variables)
        #print("DEBUG :UPDATE")
        sleep(self.waitTime)
        # clean
        client.execute(deleteMutation)
        #print("DEBUG :CLEAN")
        pass
    def __init__(self, waitTime):
        super().__init__()
        self.waitTime = waitTime
    pass
pass
