#!/usr/bin/env python3
# coding=utf-8
# import
from unittest import TestCase
from time import sleep
from threading import Thread
from test.graphQL import client as graphQLTestClient
from dummypythongraphqlserver.graphQL import schema, SubscriptionObserver
from dummypythongraphqlserver.entities import Client
# test class
class testClient(TestCase):
    dynamicQuery = """query ($readClientsInput:ReadClientsInput!,$withAddress:Boolean!,$withPurchaseOrders:Boolean!,$withOrderLines:Boolean!) {
        readClients (readClientsInput:$readClientsInput,withAddress:$withAddress,withPurchaseOrders:$withPurchaseOrders,withOrderLines:$withOrderLines) {
            firstName,
            lastName,
            address @include(if: $withAddress) {
                houseNumber,
                street,
                zipCode,
                city
            }
            purchaseOrders @include(if: $withPurchaseOrders) {
                creationDate,
                orderLines @include(if: $withOrderLines) {
                    item {
                        name,
                        price
                    },
                    quantity
                }
            }
        }
    }"""
    def testClient00(self):
        query = """{
            readClients (readClientsInput:{firstName:"Stephen"}) {
                id,
                firstName,
                lastName
            }
        }"""
        executed = graphQLTestClient.execute(query)
        expected = {'readClients': [{'id': '1', 'firstName': 'Stephen', 'lastName': 'King'}, {'id': '4', 'firstName': 'Stephen', 'lastName': 'Hawking'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testClient01(self):
        query = """{
            readClients (readClientsInput:{firstName:"Frank",lastName:"ZAPPA"}) {
                firstName,
                lastName
            }
        }"""
        executed = graphQLTestClient.execute(query)
        expected = {'readClients': [{'firstName': 'Frank', 'lastName': 'Zappa'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testClient02(self):
        query = """{
            readClients (readClientsInput:{limit:3,offset:2}) {
                id
            }
        }"""
        executed = graphQLTestClient.execute(query)
        expected = {'readClients': [{'id': '3'}, {'id': '4'}, {'id': '5'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testClient03(self):
        variables = {"readClientsInput": {"firstName": "Frank", "lastName": "ZAPPA"}, "withAddress": True, "withPurchaseOrders": False, "withOrderLines": False}
        executed = graphQLTestClient.execute(testClient.dynamicQuery,variables=variables)
        expected = {'readClients': [{'firstName': 'Frank', 'lastName': 'Zappa', 'address': {'houseNumber': '3', 'street': 'Jockey Hollow Dr.', 'zipCode': '6606', 'city': 'Bridgeport'}}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testClient04(self):
        variables = {"readClientsInput": {"firstName": "Frank", "lastName": "ZAPPA"}, "withAddress": False, "withPurchaseOrders": True, "withOrderLines": False}
        executed = graphQLTestClient.execute(testClient.dynamicQuery,variables=variables)
        expected = {'readClients': [{'firstName': 'Frank', 'lastName': 'Zappa', 'purchaseOrders': [{'creationDate': 20070308}, {'creationDate': 20121227}]}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testClient05(self):
        variables = {"readClientsInput": {"firstName": "stephen"}, "withAddress": True, "withPurchaseOrders": True, "withOrderLines": True}
        executed = graphQLTestClient.execute(testClient.dynamicQuery,variables=variables)
        expected = {'readClients': [{'firstName': 'Stephen', 'lastName': 'King', 'address': {'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee'}, 'purchaseOrders': [{'creationDate': 20080828, 'orderLines': [{'item': {'name': 'pumpkin pie', 'price': 1.58}, 'quantity': 7}, {'item': {'name': 'pumpkin seeds', 'price': 3.15}, 'quantity': 77}, {'item': {'name': 'chicken (raw)', 'price': 8.4}, 'quantity': 81}]}]}, {'firstName': 'Stephen', 'lastName': 'Hawking', 'address': {'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee'}, 'purchaseOrders': [{'creationDate': 20110306, 'orderLines': [{'item': {'name': 'porkchop (cooked)', 'price': 13.65}, 'quantity': 69}]}]}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testClient06(self):
        variables = {"readClientsInput": {"firstName": "stephen"}, "withAddress": False, "withPurchaseOrders": False, "withOrderLines": True}
        executed = graphQLTestClient.execute(testClient.dynamicQuery,variables=variables)
        expected = {'readClients': [{'firstName': 'Stephen', 'lastName': 'King'}, {'firstName': 'Stephen', 'lastName': 'Hawking'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testClient07(self):
        query = """{
            readClients (readClientsInput:{firstName:0}) {
                id
            }
        }"""
        executed = graphQLTestClient.execute(query)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "readClientsInput" has invalid value {firstName: 0}.\nIn field "firstName": Expected type "String", found 0.""")
        pass
    def testClient08(self):
        query = """{
            readClients (clientsInput_:{firstName:"Stephen"}) {
                id
            }
        }"""
        executed = graphQLTestClient.execute(query)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "clientsInput_" on field "readClients" of type "Query". Did you mean "readClientsInput"?""")
        pass
    def testClient09(self):
        # clean
        deleteMutation = """mutation {
            deleteClients(deleteClientsInput:{firstName:"first_name#", lastName:"last_name#"}) {confirmation}
        }"""
        graphQLTestClient.execute(deleteMutation)
        # create
        mutation = """mutation {
            createClients (createClientsInput:[{firstName:"first_name#0", lastName:"last_name#0", addressId:1},{firstName:"first_name#1", lastName:"last_name#1", addressId:2}]) {
                readClients {id}
            }
        }"""
        executed = graphQLTestClient.execute(mutation)
        ids=list()
        for client in executed["data"]["createClients"]["readClients"] :
            self.assertIsNotNone(client["id"])
            ids.append(client["id"])
        # update
        mutation = """mutation {
            updateClients (updateClientsInput:[{id:"""+ids[0]+""", firstName:"first_name#2", lastName:"last_name#2", addressId:3},{id:"""+ids[1]+""", firstName:"first_name#3", lastName:"last_name#3", addressId:4}]) {confirmation}
        }"""
        executed = graphQLTestClient.execute(mutation)
        self.assertTrue(executed["data"]["updateClients"]["confirmation"])
        # delete
        executed = graphQLTestClient.execute(deleteMutation)
        self.assertTrue(executed["data"]["deleteClients"]["confirmation"])
        pass
    def testClient10(self):
        # clean
        deleteMutation = """mutation deleteMutation ($deleteClientsInput:DeleteClientsInput!) {
            deleteClients(deleteClientsInput:$deleteClientsInput) {confirmation}
        }"""
        deleteVariables = {"deleteClientsInput": {"firstName":"first_name#", "lastName":"last_name#"}}
        graphQLTestClient.execute(deleteMutation,variables=deleteVariables)
        # create
        mutation = """mutation createMutation ($createClientsInput:[CreateClientsInput!]!, $withAddress:Boolean!) {
            createClients (createClientsInput:$createClientsInput, withAddress:$withAddress) {
                readClients {
                    id,
                    address @include(if: $withAddress) {id}
               }
            }
        }"""
        variables = {"createClientsInput": [{"firstName":"first_name#0", "lastName":"last_name#0", "addressId":1},{"firstName":"first_name#1", "lastName":"last_name#1", "addressId":2}], "withAddress": True}
        executed = graphQLTestClient.execute(mutation,variables=variables)
        ids=list()
        for client in executed["data"]["createClients"]["readClients"] :
            self.assertIsNotNone(client["id"])
            self.assertIsNotNone(client["address"])
            ids.append(client["id"])
        # update
        mutation = """mutation updateMutation ($updateClientsInput:[UpdateClientsInput!]!) {
            updateClients (updateClientsInput:$updateClientsInput) {confirmation}
        }"""
        variables = {"updateClientsInput": [{"id":ids[0], "firstName":"first_name#2", "lastName":"last_name#2", "addressId":3},{"id":ids[1], "firstName":"first_name#3", "lastName":"last_name#3", "addressId":4}]}
        executed = graphQLTestClient.execute(mutation,variables=variables)
        self.assertTrue(executed["data"]["updateClients"]["confirmation"])
        # delete
        executed = graphQLTestClient.execute(deleteMutation,variables=deleteVariables)
        self.assertTrue(executed["data"]["deleteClients"]["confirmation"])
        pass
    def testClient11(self):
        mutation = """mutation {
            deleteClients(deleteClientsInput:{firstName:1.0, lastName:"last_name#"}) {confirmation}
        }"""
        executed = graphQLTestClient.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "deleteClientsInput" has invalid value {firstName: 1.0, lastName: "last_name#"}.\nIn field "firstName": Expected type "String", found 1.0.""")
        pass
    def testClient12(self):
        mutation = """mutation {
            deleteClients(deleteClientsInput_:{firstName:"first_name#", lastName:"last_name#"}) {confirmation}
        }"""
        executed = graphQLTestClient.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "deleteClientsInput_" on field "deleteClients" of type "Mutation". Did you mean "deleteClientsInput"?""")
        pass
    def testClient13(self):
        mutation = """mutation {
            createClients(createClientsInput:[{firstName:1.0, lastName:"last_name#0", addressId:1}]) {Clients {id}}
        }"""
        executed = graphQLTestClient.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "createClientsInput" has invalid value [{firstName: 1.0, lastName: "last_name#0", addressId: 1}].\nIn element #0: In field "firstName": Expected type "String", found 1.0.""")
        pass
    def testClient14(self):
        mutation = """mutation {
            createClients(createClientsInput_:[{firstName:"first_name#0", lastName:"last_name#0", addressId:1}]) {Clients {id}}
        }"""
        executed = graphQLTestClient.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "createClientsInput_" on field "createClients" of type "Mutation". Did you mean "createClientsInput"?""")
        pass
    pass
    def testClient15(self):
        mutation = """mutation {
            updateClients(updateClientsInput:[{id:1, firstName:1.0, lastName:"last_name#0", addressId:1}]) {confirmation}
        }"""
        executed = graphQLTestClient.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "updateClientsInput" has invalid value [{id: 1, firstName: 1.0, lastName: "last_name#0", addressId: 1}].\nIn element #0: In field "firstName": Expected type "String", found 1.0.""")
        pass
    def testClient16(self):
        mutation = """mutation {
            updateClients(updateClientsInput_:[{id:1, firstName:"first_name#0", lastName:"last_name#0", addressId:1}]) {confirmation}
        }"""
        executed = graphQLTestClient.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "updateClientsInput_" on field "updateClients" of type "Mutation". Did you mean "updateClientsInput"?""")
        pass
    pass
    def testClient17(self):
        # subscribe
        subscription = """subscription ($readClientsInput:ReadClientsInput!,$period:Int!,$occurencesNumber:Int!,$withAddress:Boolean!,$withPurchaseOrders:Boolean!,$withOrderLines:Boolean!) {
            readClients (readClientsInput:$readClientsInput,period:$period,occurencesNumber:$occurencesNumber,withAddress:$withAddress,withPurchaseOrders:$withPurchaseOrders,withOrderLines:$withOrderLines) {
                firstName,
                lastName,
                address @include(if: $withAddress) {
                    houseNumber,
                    street,
                    zipCode,
                    city
                }
                purchaseOrders @include(if: $withPurchaseOrders) {
                    creationDate,
                    orderLines @include(if: $withOrderLines) {
                        item {
                            name,
                            price
                        },
                        quantity
                    }
                }
            }
        }"""
        '''INFO :
        the graphQLTestClient.execute method return a ordered dict, not an observable object,
        so we test subscription without test graphQLTestClient
        '''
        variables = {"readClientsInput": {"firstName":"first_name#", "lastName":"last_name#"}, "period":1, "occurencesNumber":20, "withAddress": True, "withPurchaseOrders": True, "withOrderLines": True}
        # run observer & mutation thread
        observer = TestClientsObserver(subscription, variables)
        TestClientsUpdateThread(int(observer.period*observer.occurencesNumber/4)).start()
        observable = schema.execute(subscription,variables=variables,allow_subscriptions=True)
        # INFO : timestamp decorator function increment a loop conter name 'value'
        observable.interval(observer.period*1000).timestamp().filter(lambda self : self.value < observer.occurencesNumber).subscribe(observer=observer)
        sleep(observer.period*(observer.occurencesNumber+1))
        # check results
        self.assertEqual(len(observer.results), 3)
        self.assertIn(tuple(), observer.results)
        pass
    def testClient18(self):
        # subscribe
        subscription = """subscription ($readClientsInput:ReadClientsInput!,$period:Int!,$occurencesNumber:Int!,$withAddress:Boolean!,$withPurchaseOrders:Boolean!,$withOrderLines:Boolean!) {
            readClients (readClientsInput:$readClientsInput,period:$period,occurencesNumber:$occurencesNumber,withAddress:$withAddress,withPurchaseOrders:$withPurchaseOrders,withOrderLines:$withOrderLines) {id}
        }"""
        '''INFO :
        the graphQLTestClient.execute method return a ordered dict, not an observable object,
        so we test subscription without test graphQLTestClient
        '''
        variables = {"readClientsInput": {"firstName":"first_name#", "lastName":"last_name#"}, "period":"a", "occurencesNumber":20, "withAddress": True, "withPurchaseOrders": True, "withOrderLines": True}
        # run observable
        observable = schema.execute(subscription,variables=variables,allow_subscriptions=True)
        # check results
        self.assertEqual(str(observable.errors[0]),"""could not convert string to float: 'a'""")
        pass
    pass
# test subscription
class TestClientsObserver(SubscriptionObserver):
    # methods
    @staticmethod
    def instanciate(result):
        instance = Client(firstName=result.get("firstName") ,lastName=result.get("lastName") ,id=result.get("id"))
        return instance
    # inheritance
    def on_next(self, value):
        result = super().on_next(value)
        instances = list()
        for element in result.data["readClients"]:
            instance = TestClientsObserver.instanciate(element)
            instances.append(instance)
            pass
        self.results.add(tuple(instances))
        #print("DEBUG : ClientsObserver.on_next")
    # constructor
    def __init__(self, subscription,variables):
        super().__init__(subscription,variables)
        self.results = set()
    pass
class TestClientsUpdateThread(Thread):
    def run(self):
        # clean
        deleteMutation = """mutation {
            deleteClients(deleteClientsInput:{firstName:"first_name#", lastName:"last_name#"}) {confirmation}
        }"""
        graphQLTestClient.execute(deleteMutation)
        #print("DEBUG : CLEAN")
        sleep(self.waitTime)
        # create
        mutation = """mutation createMutation ($createClientsInput:[CreateClientsInput!]!) {
            createClients (createClientsInput:$createClientsInput) {
                readClients {id}
            }
        }"""
        variables = {"createClientsInput": [{"firstName":"first_name#0", "lastName":"last_name#0", "addressId":1},{"firstName":"first_name#1", "lastName":"last_name#1", "addressId":2}]}
        executed = graphQLTestClient.execute(mutation,variables=variables)
        ids=list()
        for client in executed["data"]["createClients"]["readClients"] :
            ids.append(client["id"])
        #print("DEBUG : INSERT")
        sleep(self.waitTime)
        # update
        mutation = """mutation updateMutation ($updateClientsInput:[UpdateClientsInput!]!) {
            updateClients (updateClientsInput:$updateClientsInput) {confirmation}
        }"""
        variables = {"updateClientsInput": [{"id":ids[0], "firstName":"first_name#2", "lastName":"last_name#2", "addressId":3},{"id":ids[1], "firstName":"first_name#3", "lastName":"last_name#3", "addressId":4}]}
        graphQLTestClient.execute(mutation,variables=variables)
        #print("DEBUG : UPDATE")
        sleep(self.waitTime)
        # clean
        graphQLTestClient.execute(deleteMutation)
        #print("DEBUG : CLEAN")
        pass
    def __init__(self, waitTime):
        super().__init__()
        self.waitTime = waitTime
    pass
pass
