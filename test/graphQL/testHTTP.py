#!/usr/bin/env python3
# coding=utf-8
# import
from unittest import TestCase
from http.client import HTTPConnection
from urllib.parse import urlencode
from threading import Thread
from time import sleep
from json import loads, dumps
from http.server import BaseHTTPRequestHandler, HTTPServer
from dummypythongraphqlserver.interface.HTTP.server import HTTPRequestHandler, run as runServer
from test.graphQL.testAddress import TestAddressesUpdateThread
# test class
class testHTTP(TestCase):
    # server thread
    class ServerThread(Thread):
        # inheritance
        def run(self):
            runServer()
    # attributs
    connection = None
    serverThread = ServerThread()
    # common connections
    @staticmethod
    def getConnect(query, variables):
        parameters = urlencode({"query": query, "variables": dumps(variables)})
        testHTTP.connection.request("GET", HTTPRequestHandler.expectedPath + "?%s" % parameters)
    @staticmethod
    def postConnect(query, variables):
        body = dumps({"query": query, "variables": variables})
        testHTTP.connection.request("POST", HTTPRequestHandler.expectedPath, body)
    # threads set up
    @classmethod
    def setUpClass(cls):
        # run HTPP thread
        testHTTP.connection = HTTPConnection(HTTPRequestHandler.host, HTTPRequestHandler.port)
        testHTTP.serverThread.start()
        # run subscription test thread
        cls.callbackTestThread = CallbackTestThread()
        cls.callbackTestThread.start()
        # wait for threads to be up
        sleep(5)
    @classmethod
    def tearDownClass(cls):
        # stop HTPP thread
        testHTTP.connection.close()
        testHTTP.serverThread._tstate_lock = None
        testHTTP.serverThread._stop()
        # stop subscription test thread
        cls.callbackTestThread.httpd.shutdown()
        pass
    # tests
    def testFineQueryResponse(self):
        parametersArray = ((
            (("""{readAddresses (readAddressesInput:{street:"st."}) {id}}""", "null")),
            (("""query ($readClientsInput:ReadClientsInput!) {readClients (readClientsInput:$readClientsInput) {id}}""", """{"readClientsInput": {"firstName": "Frank", "lastName": "ZAPPA"}}""")),
            (("""mutation {deleteAddresses(deleteAddressesInput:{houseNumber:1, street:"STREET#", zipCode:10, city:"CITY#"}) {confirmation}}""", "null")),
            (("""mutation {createAddresses (createAddressesInput:[{houseNumber:10, street:"STREET#0", zipCode:100, city:"CITY#0"},{houseNumber:11, street:"STREET#1", zipCode:101, city:"CITY#1"}]) {readAddresses {id}}}""", "null")),
            (("""mutation {deleteAddresses(deleteAddressesInput:{houseNumber:1, street:"STREET#", zipCode:10, city:"CITY#"}) {confirmation}}""", "null")),
            (("""mutation deleteMutation ($deleteClientsInput:DeleteClientsInput!) {deleteClients(deleteClientsInput:$deleteClientsInput) {confirmation}}""", """{"deleteClientsInput": {"firstName":"first_name#", "lastName":"last_name#"}}""")),
            (("""mutation createMutation ($createClientsInput:[CreateClientsInput!]!) {createClients (createClientsInput:$createClientsInput) {readClients {id}}}""", """{"createClientsInput": [{"firstName":"first_name#0", "lastName":"last_name#0", "addressId":1},{"firstName":"first_name#1", "lastName":"last_name#1", "addressId":2}]}""")),
            (("""mutation deleteMutation ($deleteClientsInput:DeleteClientsInput!) {deleteClients(deleteClientsInput:$deleteClientsInput) {confirmation}}""", """{"deleteClientsInput": {"firstName":"first_name#", "lastName":"last_name#"}}""")),
        ))
        # INFO separate GET & POST tests just to ensure no duplicated inserts
        for parametersLine in parametersArray:
            parameters = urlencode({"query" : parametersLine[0],"variables" : parametersLine[1]})
            testHTTP.connection.request("GET", HTTPRequestHandler.expectedPath + "?%s" % parameters)
            self.assertFineQueryResponse()
        for parametersLine in parametersArray:
            body = dumps({"query" : parametersLine[0],"variables" : loads(parametersLine[1])})
            testHTTP.connection.request("POST", HTTPRequestHandler.expectedPath , body)
            self.assertFineQueryResponse()
        pass
    def assertFineQueryResponse(self):
        response = loads(testHTTP.connection.getresponse().read().decode())
        self.assertIn("data", response)
        self.assertGreater(len(response["data"]), 0)
        self.assertNotIn("errors", response)
    def testPathError(self):
        self.assertPathError("GET")
        self.assertPathError("POST")
    def assertPathError(self, verb):
        testHTTP.connection.request(verb,"/")
        response = loads(testHTTP.connection.getresponse().read().decode())
        self.assertNotIn("data", response)
        self.assertIn("errors", response)
        self.assertEqual(len(response["errors"]), 1)
        self.assertEqual(response["errors"][0], "Expected path : " + HTTPRequestHandler.expectedPath)
    def testQueryError(self):
        query = """{readAddresses (readAddressesInput:{street:1}) {id}}"""
        parameters = urlencode({"query": query, "variables": "null"})
        testHTTP.connection.request("GET", HTTPRequestHandler.expectedPath + "?%s" % parameters)
        self.assertQueryError()
        body = dumps({"query": query, "variables": None})
        testHTTP.connection.request("POST", HTTPRequestHandler.expectedPath, body)
        self.assertQueryError()
    def assertQueryError(self):
        response = loads(testHTTP.connection.getresponse().read().decode())
        self.assertNotIn("data", response)
        self.assertIn("errors", response)
        self.assertEqual(len(response["errors"]), 1)
        self.assertEqual(response["errors"][0], """Argument "readAddressesInput" has invalid value {street: 1}.\nIn field "street": Expected type "String", found 1.""")
    def testQueryVariablesError(self):
        query = """query ($readClientsInput:ReadClientsInput!) {readClients (readClientsInput:$readClientsInput) {id}}"""
        variables = """{"_readClientsInput": {"firstName": "Frank", "lastName": "ZAPPA"}}"""
        parameters = urlencode({"query": query, "variables": variables})
        testHTTP.connection.request("GET", HTTPRequestHandler.expectedPath + "?%s" % parameters)
        self.assertQueryVariablesError()
        body = dumps({"query": query, "variables": loads(variables)})
        testHTTP.connection.request("POST", HTTPRequestHandler.expectedPath, body)
        self.assertQueryVariablesError()
    def assertQueryVariablesError(self):
        response = loads(testHTTP.connection.getresponse().read().decode())
        self.assertNotIn("data", response)
        self.assertIn("errors", response)
        self.assertEqual(len(response["errors"]), 1)
        self.assertEqual(response["errors"][0], """Variable "$readClientsInput" of required type "ReadClientsInput!" was not provided.""")
    def testFineSubscriptionGet(self):
        self.assertFineSubscription(testHTTP.getConnect)
    def testFineSubscriptionPost(self):
        self.assertFineSubscription(testHTTP.postConnect)
    def assertFineSubscription(self,connect):
        # subscribe
        query = """subscription ($readAddressesInput:ReadAddressesInput!,$period:Int!,$occurencesNumber:Int!,$callbackEndpoint:String) {
            readAddresses (readAddressesInput:$readAddressesInput,period:$period,occurencesNumber:$occurencesNumber,callbackEndpoint:$callbackEndpoint) {id}
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        period = 1
        occurencesNumber = 20
        variables = {"readAddressesInput": {"houseNumber":1, "street":"STREET#", "zipCode":10, "city":"CITY#"}, "period":period, "occurencesNumber":occurencesNumber, "callbackEndpoint" : "http://localhost:8082/"}
        # run queries
        connect(query,variables)
        testHTTP.connection.getresponse()
        testAddressesUpdateThread = TestAddressesUpdateThread(int(period*occurencesNumber/4))
        testAddressesUpdateThread.start()
        sleep(period*(occurencesNumber+1))
        # check results
        response = loads(list(CallbackTestHTTPHandler.results)[1])
        self.assertIn("data",response)
        self.assertIn("readAddresses",response["data"])
        pass
    def testSubscriptionQueryError(self):
        query = """subscription ($readAddressesInput:ReadAddressesInput!,$period:Int!,$occurencesNumber:Int!,$callbackEndpoint:String) {
            readAddresses (_readAddressesInput:$readAddressesInput,period:$period,occurencesNumber:$occurencesNumber,callbackEndpoint:$callbackEndpoint) {id}
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        variables = {"readAddresses": {"houseNumber":1, "street":"STREET#", "zipCode":10, "city":"CITY#"}, "period":1, "occurencesNumber":20, "callbackEndpoint" : "http://localhost:8082/"}
        self.assertSubscriptionQueryError(query,variables,testHTTP.getConnect)
        self.assertSubscriptionQueryError(query,variables,testHTTP.postConnect)
    def assertSubscriptionQueryError(self,query,variables,connect):
        response = self.assertSubscriptionError(query,variables,connect)
        self.assertEqual(response["errors"][0], """Unknown argument "_readAddressesInput" on field "readAddresses" of type "Subscription". Did you mean "readAddressesInput"?""")
        pass
    def testSubscriptionVariablesError(self):
        query = """subscription ($readAddressesInput:ReadAddressesInput!,$period:Int!,$occurencesNumber:Int!,$callbackEndpoint:String) {
            readAddresses (readAddressesInput:$readAddressesInput,period:$period,occurencesNumber:$occurencesNumber,callbackEndpoint:$callbackEndpoint) {id}
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        variables = {"readAddressesInput": {"houseNumber":"a", "street":"STREET#", "zipCode":10, "city":"CITY#"}, "period":1, "occurencesNumber":20, "callbackEndpoint" : "http://localhost:8082/"}
        self.assertSubscriptionVariablesError(query,variables,testHTTP.getConnect)
        self.assertSubscriptionVariablesError(query,variables,testHTTP.postConnect)
    def assertSubscriptionVariablesError(self,query,variables,connect):
        response = self.assertSubscriptionError(query,variables,connect)
        self.assertEqual(response["errors"][0], """could not convert string to float: 'a'""")
        pass
    def assertSubscriptionError(self, query,variables,connect):
        # parse variables
        period = variables["period"]
        occurencesNumber = variables["occurencesNumber"]
        # run queries
        connect(query,variables)
        testHTTP.connection.getresponse()
        sleep(period*(occurencesNumber+1))
        # check results
        response = loads(list(CallbackTestHTTPHandler.results)[1])
        self.assertIn("errors",response)
        return response
    def testSubscriptionCallbackErrorGet(self):
        self.assertSubscriptionCallbackError(testHTTP.getConnect)
    def testSubscriptionCallbackErrorPost(self):
        self.assertSubscriptionCallbackError(testHTTP.postConnect)
    def assertSubscriptionCallbackError(self,connect):
        # subscribe
        query = """subscription ($readAddressesInput:ReadAddressesInput!,$period:Int!,$occurencesNumber:Int!,$callbackEndpoint:String) {
            readAddresses (readAddressesInput:$readAddressesInput,period:$period,occurencesNumber:$occurencesNumber,callbackEndpoint:$callbackEndpoint) {id}
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        callbackEndpoint = "http://localhost:1234/"
        variables = {"readAddressesInput": {"houseNumber":1, "street":"STREET#", "zipCode":10, "city":"CITY#"}, "period":0, "occurencesNumber":0, "callbackEndpoint" : callbackEndpoint}
        # run queries
        connect(query,variables)
        response = loads(testHTTP.connection.getresponse().read().decode())
        # check results
        self.assertNotIn("data",response)
        self.assertIn("errors",response)
        self.assertEqual(response["errors"][0], "Your callbackendpoint ("+callbackEndpoint+") is not responding")
        pass
    pass
# test callback
class CallbackTestHTTPHandler(BaseHTTPRequestHandler):
    host = '0.0.0.0'
    port = 8082
    results = set()
    def do_POST(self):
        self.send_response(200)
        self.end_headers()
        bodyLength = int(self.headers.get("content-length"))
        content = self.rfile.read(bodyLength).decode()
        CallbackTestHTTPHandler.results.add(content)
        pass
    pass
class CallbackTestThread(Thread,BaseHTTPRequestHandler):
    def run(self):
        self.httpd.serve_forever()
    def __init__(self):
        super().__init__()
        server_address = (CallbackTestHTTPHandler.host, CallbackTestHTTPHandler.port)
        self.httpd = HTTPServer(server_address, CallbackTestHTTPHandler)
    pass
pass