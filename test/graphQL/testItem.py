#!/usr/bin/env python3
# coding=utf-8
# import
from unittest import TestCase
from time import sleep
from threading import Thread
from test.graphQL import client
from dummypythongraphqlserver.graphQL import schema, SubscriptionObserver
from dummypythongraphqlserver.entities import Item
# test class
class testItem(TestCase):
    dynamicQuery = """query ($readItemsInput:ReadItemsInput!,$withClients:Boolean!,$withAddress:Boolean!,$withPurchaseOrders:Boolean!) {
        readItems (readItemsInput:$readItemsInput,withClients:$withClients,withAddress:$withAddress,withPurchaseOrders:$withPurchaseOrders) {
            name,
            price,
            clients @include(if: $withClients) {
                firstName,
                lastName,
                purchaseOrders @include(if: $withPurchaseOrders) {
                    creationDate,
                    quantity
                },
                address @include(if: $withAddress) {
                    houseNumber,
                    street,
                    zipCode,
                    city
                }
            }
        }
    }"""
    def testItem00(self):
        query = """{
            readItems (readItemsInput:{name:"Chicken"}) {
                id,
                name,
                price
            }
        }"""
        executed = client.execute(query)
        expected = {'readItems': [{'id': '1', 'name': 'chicken (raw)', 'price': 8.4}, {'id': '2', 'name': 'chicken (cooked)', 'price': 9.45}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testItem01(self):
        query = """{
            readItems (readItemsInput:{name:"Porkchop",price:".6"}) {
                name,
                price
            }
        }"""
        executed = client.execute(query)
        expected = {'readItems': [{'name': 'porkchop (raw)', 'price': 12.6}, {'name': 'porkchop (cooked)', 'price': 13.65}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testItem02(self):
        query = """{
            readItems (readItemsInput:{limit:3,offset:2}) {
                id
            }
        }"""
        executed = client.execute(query)
        expected = {'readItems': [{'id': '3'}, {'id': '4'}, {'id': '5'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testItem03(self):
        variables = {"readItemsInput": {"name": "Porkchop", "price": ".6"}, "withClients": True, "withAddress": False, "withPurchaseOrders": False}
        executed = client.execute(testItem.dynamicQuery,variables=variables)
        expected = {'readItems': [{'name': 'porkchop (raw)', 'price': 12.6, 'clients': []}, {'name': 'porkchop (cooked)', 'price': 13.65, 'clients': [{'firstName': 'Stephen', 'lastName': 'Hawking'}, {'firstName': 'Toby', 'lastName': 'Keith'}]}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testItem04(self):
        variables = {"readItemsInput": {"name": "Porkchop", "price": ".6"}, "withClients": False, "withAddress": True, "withPurchaseOrders": False}
        executed = client.execute(testItem.dynamicQuery,variables=variables)
        expected = {'readItems': [{'name': 'porkchop (raw)', 'price': 12.6}, {'name': 'porkchop (cooked)', 'price': 13.65}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testItem05(self):
        variables = {"readItemsInput": {"name": "chicken"}, "withClients": True, "withAddress": True, "withPurchaseOrders": True}
        executed = client.execute(testItem.dynamicQuery,variables=variables)
        expected = {'readItems': [{'name': 'chicken (raw)', 'price': 8.4, 'clients': [{'firstName': 'Stephen', 'lastName': 'King', 'purchaseOrders': [{'creationDate': 20080828, 'quantity': 7}], 'address': {'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee'}}, {'firstName': 'Frank', 'lastName': 'Zappa', 'purchaseOrders': [{'creationDate': 20070308, 'quantity': 38}], 'address': {'houseNumber': '3', 'street': 'Jockey Hollow Dr.', 'zipCode': '6606', 'city': 'Bridgeport'}}, {'firstName': 'Ben', 'lastName': 'Stiller', 'purchaseOrders': [{'creationDate': 20120118, 'quantity': 39}], 'address': {'houseNumber': '42', 'street': 'Annadale Lane', 'zipCode': '28655', 'city': 'Morganton'}}, {'firstName': 'Charles', 'lastName': 'Darwin', 'purchaseOrders': [{'creationDate': 20060607, 'quantity': 9}], 'address': {'houseNumber': '3', 'street': 'Jockey Hollow Dr.', 'zipCode': '6606', 'city': 'Bridgeport'}}, {'firstName': 'Toby', 'lastName': 'Keith', 'purchaseOrders': [{'creationDate': 20151210, 'quantity': 2}], 'address': {'houseNumber': '5', 'street': 'Linda Street', 'zipCode': '76039', 'city': 'Euless'}}]}, {'name': 'chicken (cooked)', 'price': 9.45, 'clients': [{'firstName': 'Dave', 'lastName': 'Matthews', 'purchaseOrders': [{'creationDate': 20030617, 'quantity': 18}], 'address': {'houseNumber': '9280', 'street': 'Water Court', 'zipCode': '37803', 'city': 'Maryville'}}, {'firstName': 'Toby', 'lastName': 'Keith', 'purchaseOrders': [{'creationDate': 20061215, 'quantity': 41}], 'address': {'houseNumber': '5', 'street': 'Linda Street', 'zipCode': '76039', 'city': 'Euless'}}]}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testItem06(self):
        variables = {"readItemsInput": {"name": "chicken"}, "withClients": False, "withAddress": False, "withPurchaseOrders": True}
        executed = client.execute(testItem.dynamicQuery,variables=variables)
        expected = {'readItems': [{'name': 'chicken (raw)', 'price': 8.4}, {'name': 'chicken (cooked)', 'price': 9.45}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testItem07(self):
        query = """{
            readItems (readItemsInput:{name:0}) {
                id
            }
        }"""
        executed = client.execute(query)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "readItemsInput" has invalid value {name: 0}.\nIn field "name": Expected type "String", found 0.""")
        pass
    def testItem08(self):
        query = """{
            readItems (itemsInput_:{name:"Chicken"}) {
                id
            }
        }"""
        executed = client.execute(query)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "itemsInput_" on field "readItems" of type "Query". Did you mean "readItemsInput"?""")
        pass
    def testItem09(self):
        # clean
        deleteMutation = """mutation {
            deleteItems(deleteItemsInput:{name:"name#", price:"1"}) {confirmation}
        }"""
        client.execute(deleteMutation)
        # create
        mutation = """mutation {
            createItems (createItemsInput:[{name:"name#0", price:"1.0"},{name:"name#1", price:"1.1"}]) {
                readItems {id}
            }
        }"""
        executed = client.execute(mutation)
        ids=list()
        for item in executed["data"]["createItems"]["readItems"] :
            self.assertIsNotNone(item["id"])
            ids.append(item["id"])
        # update
        mutation = """mutation {
            updateItems (updateItemsInput:[{id:"""+ids[0]+""", name:"name#2", price:"1.2"},{id:"""+ids[1]+""", name:"name#3", price:"1.3"}]) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertTrue(executed["data"]["updateItems"]["confirmation"])
        # delete
        executed = client.execute(deleteMutation)
        self.assertTrue(executed["data"]["deleteItems"]["confirmation"])
        pass
    def testItem10(self):
        # clean
        deleteMutation = """mutation deleteMutation ($deleteItemsInput:DeleteItemsInput!) {
            deleteItems(deleteItemsInput:$deleteItemsInput) {confirmation}
        }"""
        deleteVariables = {"deleteItemsInput": {"name":"name#", "price":"1"}}
        client.execute(deleteMutation,variables=deleteVariables)
        # create
        mutation = """mutation createMutation ($createItemsInput:[CreateItemsInput!]!) {
            createItems (createItemsInput:$createItemsInput) {
                readItems {id}
            }
        }"""
        variables = {"createItemsInput": [{"name":"name#0", "price":"1.0"},{"name":"name#1", "price":"1.1"}]}
        executed = client.execute(mutation,variables=variables)
        ids=list()
        for item in executed["data"]["createItems"]["readItems"] :
            self.assertIsNotNone(item["id"])
            ids.append(item["id"])
        # update
        mutation = """mutation updateMutation ($updateItemsInput:[UpdateItemsInput!]!) {
            updateItems (updateItemsInput:$updateItemsInput) {confirmation}
        }"""
        variables = {"updateItemsInput": [{"id":ids[0], "name":"name#2", "price":"1.2"},{"id":ids[1], "name":"name#3", "price":"1.3"}]}
        executed = client.execute(mutation,variables=variables)
        self.assertTrue(executed["data"]["updateItems"]["confirmation"])
        # delete
        executed = client.execute(deleteMutation,variables=deleteVariables)
        self.assertTrue(executed["data"]["deleteItems"]["confirmation"])
        pass
    def testItem11(self):
        mutation = """mutation {
            deleteItems(deleteItemsInput:{name:0, price:"1"}) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "deleteItemsInput" has invalid value {name: 0, price: "1"}.\nIn field "name": Expected type "String", found 0.""")
        pass
    def testItem12(self):
        mutation = """mutation {
            deleteItems(deleteItemsInput_:{name:"name#", price:"1"}) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "deleteItemsInput_" on field "deleteItems" of type "Mutation". Did you mean "deleteItemsInput"?""")
        pass
    def testItem13(self):
        mutation = """mutation {
            createItems(createItemsInput:[{name:0, price:"1.0"}]) {Items {id}}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "createItemsInput" has invalid value [{name: 0, price: "1.0"}].\nIn element #0: In field "name": Expected type "String", found 0.""")
        pass
    def testItem14(self):
        mutation = """mutation {
            createItems(createItemsInput_:[{name:"name#0", price:"1.0"}]) {Items {id}}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "createItemsInput_" on field "createItems" of type "Mutation". Did you mean "createItemsInput"?""")
        pass
    pass
    def testItem15(self):
        mutation = """mutation {
            updateItems(updateItemsInput:[{id: 1, name:0, price:"1.0"}]) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "updateItemsInput" has invalid value [{id: 1, name: 0, price: "1.0"}].\nIn element #0: In field "name": Expected type "String", found 0.""")
        pass
    def testItem16(self):
        mutation = """mutation {
            updateItems(updateItemsInput_:[{id: 1, name:"name#0", price:"1.0"}]) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "updateItemsInput_" on field "updateItems" of type "Mutation". Did you mean "updateItemsInput"?""")
        pass
    pass
    def testItem17(self):
        # subscribe
        subscription = """subscription ($readItemsInput:ReadItemsInput!,$period:Int!,$occurencesNumber:Int!,$withClients:Boolean!,$withAddress:Boolean!,$withPurchaseOrders:Boolean!) {
            readItems (readItemsInput:$readItemsInput,period:$period,occurencesNumber:$occurencesNumber,withClients:$withClients,withAddress:$withAddress,withPurchaseOrders:$withPurchaseOrders) {
                name,
                price,
                clients @include(if: $withClients) {
                    firstName,
                    lastName,
                    purchaseOrders @include(if: $withPurchaseOrders) {
                        creationDate,
                        quantity
                    },
                    address @include(if: $withAddress) {
                        houseNumber,
                        street,
                        zipCode,
                        city
                    }
                }
            }
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        variables = {"readItemsInput": {"name":"name#"}, "period":1, "occurencesNumber":20, "withClients": True, "withAddress": True, "withPurchaseOrders": True}
        # run observer & mutation thread
        observer = TestItemsObserver(subscription, variables)
        TestItemsUpdateThread(int(observer.period*observer.occurencesNumber/4)).start()
        observable = schema.execute(subscription,variables=variables,allow_subscriptions=True)
        # INFO : timestamp decorator function increment a loop conter name 'value'
        observable.interval(observer.period*1000).timestamp().filter(lambda self : self.value < observer.occurencesNumber).subscribe(observer=observer)
        sleep(observer.period*(observer.occurencesNumber+1))
        # check results
        self.assertEqual(len(observer.results), 3)
        self.assertIn(tuple(), observer.results)
        pass
    def testItem18(self):
        # subscribe
        subscription = """subscription ($readItemsInput:ReadItemsInput!,$period:Int!,$occurencesNumber:Int!,$withClients:Boolean!,$withAddress:Boolean!,$withPurchaseOrders:Boolean!) {
            readItems (readItemsInput:$readItemsInput,period:$period,occurencesNumber:$occurencesNumber,withClients:$withClients,withAddress:$withAddress,withPurchaseOrders:$withPurchaseOrders) {id}
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        variables = {"readItemsInput": {"name":"name#"}, "period":"a", "occurencesNumber":20, "withClients": True, "withAddress": True, "withPurchaseOrders": True}
        # run observable
        observable = schema.execute(subscription,variables=variables,allow_subscriptions=True)
        # check results
        self.assertEqual(str(observable.errors[0]),"""could not convert string to float: 'a'""")
        pass
    pass
# test subscription
class TestItemsObserver(SubscriptionObserver):
    # methods
    @staticmethod
    def instanciate(result):
        instance = Item(name=result.get("name") ,price=result.get("price") ,id=result.get("id"))
        return instance
    # inheritance
    def on_next(self, value):
        result = super().on_next(value)
        instances = list()
        for element in result.data["readItems"]:
            instance = TestItemsObserver.instanciate(element)
            instances.append(instance)
            pass
        self.results.add(tuple(instances))
        #print("DEBUG : ItemsObserver.on_next")
    # constructor
    def __init__(self, subscription,variables):
        super().__init__(subscription,variables)
        self.results = set()
    pass
class TestItemsUpdateThread(Thread):
    def run(self):
        # clean
        deleteMutation = """mutation {
            deleteItems(deleteItemsInput:{name:"name#"}) {confirmation}
        }"""
        client.execute(deleteMutation)
        #print("DEBUG : CLEAN")
        sleep(self.waitTime)
        # create
        mutation = """mutation createMutation ($createItemsInput:[CreateItemsInput!]!) {
            createItems (createItemsInput:$createItemsInput) {
                readItems {id}
            }
        }"""
        variables = {"createItemsInput": [{"name":"name#0", "price":"1.0"},{"name":"name#1", "price":"3.2"}]}
        executed = client.execute(mutation,variables=variables)
        ids=list()
        for item in executed["data"]["createItems"]["readItems"] :
            ids.append(item["id"])
        #print("DEBUG : INSERT")
        sleep(self.waitTime)
        # update
        mutation = """mutation updateMutation ($updateItemsInput:[UpdateItemsInput!]!) {
            updateItems (updateItemsInput:$updateItemsInput) {confirmation}
        }"""
        variables = {"updateItemsInput": [{"id":ids[0], "name":"name#2", "price":"5.4"},{"id":ids[1], "name":"name#3", "price":"7.6"}]}
        client.execute(mutation,variables=variables)
        #print("DEBUG : UPDATE")
        sleep(self.waitTime)
        # clean
        client.execute(deleteMutation)
        #print("DEBUG : CLEAN")
        pass
    def __init__(self, waitTime):
        super().__init__()
        self.waitTime = waitTime
    pass
pass
