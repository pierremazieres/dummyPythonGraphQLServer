#!/usr/bin/env python3
# coding=utf-8
# import
from unittest import TestCase
from test.graphQL import client
# test class
class testExtra(TestCase):
    def testComplex(self):
        query = """query complexExample {
            adresse0 : readAddresses (readAddressesInput:{street:"st."}) {
                ...commonAddresses
            }
            adresse1 : readAddresses (readAddressesInput:{city:"wood"}) {
                ...commonAddresses
            }
            readAddresses (readAddressesInput:{limit:3,offset:2}) {
                id
            }
            clientA : readClients (readClientsInput:{firstName:"Stephen"}) {
                ...commonClients
            }
            clientB : readClients (readClientsInput:{firstName:"Frank",lastName:"ZAPPA"}) {
                ...commonClients
            }
            readClients (readClientsInput:{limit:3,offset:2}) {...specificClients}
        }
        fragment commonAddresses on RootAddressField {
            houseNumber,
            street,
            zipCode,
            city
        }
        fragment commonClients on RootClientField {
                firstName,
                lastName
        }
        fragment specificClients on RootClientField {id}
        """
        executed = client.execute(query)
        expected = {'adresse0': [{'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee'}, {'houseNumber': '7763', 'street': 'Bedford St.', 'zipCode': '29646', 'city': 'Greenwood'}, {'houseNumber': '23', 'street': 'Ridgeview St.', 'zipCode': '80123', 'city': 'Littleton'}, {'houseNumber': '938', 'street': 'Hanover St.', 'zipCode': '30134', 'city': 'Douglasville'}, {'houseNumber': '7', 'street': 'High St.', 'zipCode': '7032', 'city': 'Kearny'}, {'houseNumber': '81', 'street': 'Aspen St.', 'zipCode': '33823', 'city': 'Auburndale'}], 'adresse1': [{'houseNumber': '8024', 'street': 'Cleveland Street', 'zipCode': '7040', 'city': 'Maplewood'}, {'houseNumber': '7763', 'street': 'Bedford St.', 'zipCode': '29646', 'city': 'Greenwood'}], 'readAddresses': [{'id': '3'}, {'id': '4'}, {'id': '5'}], 'clientA': [{'firstName': 'Stephen', 'lastName': 'King'}, {'firstName': 'Stephen', 'lastName': 'Hawking'}], 'clientB': [{'firstName': 'Frank', 'lastName': 'Zappa'}], 'readClients': [{'id': '3'}, {'id': '4'}, {'id': '5'}]}
        self.assertEqual(executed["data"], expected)
    def testIntrospection(self):
        query = """{
            __schema {
                types {
                    name
                }
            }
            # address
            ## query & mutation
            CreateAddresses : __type(name: "CreateAddresses") {...instrospection}
            ReadAddresses : __type(name: "ReadAddresses") {...instrospection}
            UpdateAddresses : __type(name: "UpdateAddresses") {...instrospection}
            DeleteAddresses : __type(name: "DeleteAddresses") {...instrospection}
            ## input
            CreateAddressesInput : __type(name: "CreateAddressesInput") {...instrospection}
            ReadAddressesInput : __type(name: "ReadAddressesInput") {...instrospection}
            UpdateAddressesInput : __type(name: "UpdateAddressesInput") {...instrospection}
            DeleteAddressesInput : __type(name: "DeleteAddressesInput") {...instrospection}
            ## output
            RootAddressField : __type(name: "RootAddressField") {...instrospection}
            LeafAddressField : __type(name: "LeafAddressField") {...instrospection}
            #-----
            # client
            ## query & mutation
            CreateClients : __type(name: "CreateClients") {...instrospection}
            ReadClients : __type(name: "ReadClients") {...instrospection}
            UpdateClients : __type(name: "UpdateClients") {...instrospection}
            DeleteClients : __type(name: "DeleteClients") {...instrospection}
            ## input
            CreateClientsInput : __type(name: "CreateClientsInput") {...instrospection}
            ReadClientsInput : __type(name: "ReadClientsInput") {...instrospection}
            UpdateClientsInput : __type(name: "UpdateClientsInput") {...instrospection}
            DeleteClientsInput : __type(name: "DeleteClientsInput") {...instrospection}
            ClientAddressField : __type(name: "ClientAddressField") {...instrospection}
            ## output
            RootClientField : __type(name: "RootClientField") {...instrospection}
            NodeClientField : __type(name: "NodeClientField") {...instrospection}
            LeafClientField : __type(name: "LeafClientField") {...instrospection}
            #-----
            # purchase order
            ## query & mutation
            CreatePurchaseOrders : __type(name: "CreatePurchaseOrders") {...instrospection}
            ReadPurchaseOrders : __type(name: "ReadPurchaseOrders") {...instrospection}
            UpdatePurchaseOrders : __type(name: "UpdatePurchaseOrders") {...instrospection}
            DeletePurchaseOrders : __type(name: "DeletePurchaseOrders") {...instrospection}
            ## input
            CreatePurchaseOrdersInput : __type(name: "CreatePurchaseOrdersInput") {...instrospection}
            ReadPurchaseOrdersInput : __type(name: "ReadPurchaseOrdersInput") {...instrospection}
            UpdatePurchaseOrdersInput : __type(name: "UpdatePurchaseOrdersInput") {...instrospection}
            DeletePurchaseOrdersInput : __type(name: "DeletePurchaseOrdersInput") {...instrospection}
            PurchaseOrderClientField : __type(name: "PurchaseOrderClientField") {...instrospection}
            ## output
            RootPurchaseOrderField : __type(name: "RootPurchaseOrderField") {...instrospection}
            NodePurchaseOrderField : __type(name: "NodePurchaseOrderField") {...instrospection}
            LeafPurchaseOrderField : __type(name: "LeafPurchaseOrderField") {...instrospection}
            #-----
            # order line
            ## input
            CreateOrderLineInput : __type(name: "CreateOrderLineInput") {...instrospection}
            UpdateOrderLineInput : __type(name: "UpdateOrderLineInput") {...instrospection}
            ## output
            NodeOrderLineField : __type(name: "NodeOrderLineField") {...instrospection}
            #-----
            # item
            ## query & mutation
            CreateItems : __type(name: "CreateItems") {...instrospection}
            ReadItems : __type(name: "ReadItems") {...instrospection}
            UpdateItems : __type(name: "UpdateItems") {...instrospection}
            DeleteItems : __type(name: "DeleteItems") {...instrospection}
            ## input
            CreateItemsInput : __type(name: "CreateItemsInput") {...instrospection}
            ReadItemsInput : __type(name: "ReadItemsInput") {...instrospection}
            UpdateItemsInput : __type(name: "UpdateItemsInput") {...instrospection}
            DeleteItemsInput : __type(name: "DeleteItemsInput") {...instrospection}
            ReadItemsInput : __type(name: "ReadItemsInput") {...instrospection}
            ## output
            RootItemField : __type(name: "RootItemField") {...instrospection}
            LeafItemField : __type(name: "LeafItemField") {...instrospection}
        }
        fragment instrospection on __Type {
            name
            kind
            ofType {
                name
                kind
            }
            fields {
                name
                type {
                    name
                    kind
                    ofType {
                        name
                        kind
                    }
                }
            }
        }"""
        executed = client.execute(query)
        expected = {'__schema': {'types': [{'name': 'Query'}, {'name': 'RootClientField'}, {'name': 'ID'}, {'name': 'String'}, {'name': 'LeafAddressField'}, {'name': 'NodePurchaseOrderField'}, {'name': 'Int'}, {'name': 'NodeOrderLineField'}, {'name': 'LeafItemField'}, {'name': 'Float'}, {'name': 'ReadClientsInput'}, {'name': 'Boolean'}, {'name': 'RootAddressField'}, {'name': 'ClientAddressField'}, {'name': 'ReadAddressesInput'}, {'name': 'RootPurchaseOrderField'}, {'name': 'NodeClientField'}, {'name': 'ReadPurchaseOrdersInput'}, {'name': 'RootItemField'}, {'name': 'ClientItemField'}, {'name': 'PurchaseOrderClientField'}, {'name': 'ReadItemsInput'}, {'name': 'Mutation'}, {'name': 'DeleteClients'}, {'name': 'DeleteClientsInput'}, {'name': 'CreateClients'}, {'name': 'CreateClientsInput'}, {'name': 'UpdateClients'}, {'name': 'UpdateClientsInput'}, {'name': 'DeleteAddresses'}, {'name': 'DeleteAddressesInput'}, {'name': 'CreateAddresses'}, {'name': 'CreateAddressesInput'}, {'name': 'UpdateAddresses'}, {'name': 'UpdateAddressesInput'}, {'name': 'DeletePurchaseOrders'}, {'name': 'DeletePurchaseOrdersInput'}, {'name': 'CreatePurchaseOrders'}, {'name': 'CreatePurchaseOrdersInput'}, {'name': 'CreateOrderLineInput'}, {'name': 'UpdatePurchaseOrders'}, {'name': 'UpdatePurchaseOrdersInput'}, {'name': 'UpdateOrderLineInput'}, {'name': 'DeleteItems'}, {'name': 'DeleteItemsInput'}, {'name': 'CreateItems'}, {'name': 'CreateItemsInput'}, {'name': 'UpdateItems'}, {'name': 'UpdateItemsInput'}, {'name': 'Subscription'}, {'name': '__Schema'}, {'name': '__Type'}, {'name': '__TypeKind'}, {'name': '__Field'}, {'name': '__InputValue'}, {'name': '__EnumValue'}, {'name': '__Directive'}, {'name': '__DirectiveLocation'}]}, 'CreateAddresses': {'name': 'CreateAddresses', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'readAddresses', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, 'ReadAddresses': None, 'UpdateAddresses': {'name': 'UpdateAddresses', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'confirmation', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}]}, 'DeleteAddresses': {'name': 'DeleteAddresses', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'confirmation', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}]}, 'CreateAddressesInput': {'name': 'CreateAddressesInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'ReadAddressesInput': {'name': 'ReadAddressesInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'UpdateAddressesInput': {'name': 'UpdateAddressesInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'DeleteAddressesInput': {'name': 'DeleteAddressesInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'RootAddressField': {'name': 'RootAddressField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'houseNumber', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'street', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'zipCode', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'city', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'clients', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, 'LeafAddressField': {'name': 'LeafAddressField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'houseNumber', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'street', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'zipCode', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'city', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}]}, 'CreateClients': {'name': 'CreateClients', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'readClients', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, 'ReadClients': None, 'UpdateClients': {'name': 'UpdateClients', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'confirmation', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}]}, 'DeleteClients': {'name': 'DeleteClients', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'confirmation', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}]}, 'CreateClientsInput': {'name': 'CreateClientsInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'ReadClientsInput': {'name': 'ReadClientsInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'UpdateClientsInput': {'name': 'UpdateClientsInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'DeleteClientsInput': {'name': 'DeleteClientsInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'ClientAddressField': {'name': 'ClientAddressField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'firstName', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'lastName', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'purchaseOrders', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, 'RootClientField': {'name': 'RootClientField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'firstName', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'lastName', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'address', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'LeafAddressField', 'kind': 'OBJECT'}}}, {'name': 'purchaseOrders', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, 'NodeClientField': {'name': 'NodeClientField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'firstName', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'lastName', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'address', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'LeafAddressField', 'kind': 'OBJECT'}}}]}, 'LeafClientField': None, 'CreatePurchaseOrders': {'name': 'CreatePurchaseOrders', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'readPurchaseOrders', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, 'ReadPurchaseOrders': None, 'UpdatePurchaseOrders': {'name': 'UpdatePurchaseOrders', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'confirmation', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}]}, 'DeletePurchaseOrders': {'name': 'DeletePurchaseOrders', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'confirmation', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}]}, 'CreatePurchaseOrdersInput': {'name': 'CreatePurchaseOrdersInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'ReadPurchaseOrdersInput': {'name': 'ReadPurchaseOrdersInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'UpdatePurchaseOrdersInput': {'name': 'UpdatePurchaseOrdersInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'DeletePurchaseOrdersInput': {'name': 'DeletePurchaseOrdersInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'PurchaseOrderClientField': {'name': 'PurchaseOrderClientField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'creationDate', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Int', 'kind': 'SCALAR'}}}, {'name': 'quantity', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Int', 'kind': 'SCALAR'}}}]}, 'RootPurchaseOrderField': {'name': 'RootPurchaseOrderField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'creationDate', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Int', 'kind': 'SCALAR'}}}, {'name': 'orderLines', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}, {'name': 'client', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'NodeClientField', 'kind': 'OBJECT'}}}]}, 'NodePurchaseOrderField': {'name': 'NodePurchaseOrderField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'creationDate', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Int', 'kind': 'SCALAR'}}}, {'name': 'orderLines', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, 'LeafPurchaseOrderField': None, 'CreateOrderLineInput': {'name': 'CreateOrderLineInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'UpdateOrderLineInput': {'name': 'UpdateOrderLineInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'NodeOrderLineField': {'name': 'NodeOrderLineField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'item', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'LeafItemField', 'kind': 'OBJECT'}}}, {'name': 'quantity', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Int', 'kind': 'SCALAR'}}}]}, 'CreateItems': {'name': 'CreateItems', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'readItems', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, 'ReadItems': None, 'UpdateItems': {'name': 'UpdateItems', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'confirmation', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}]}, 'DeleteItems': {'name': 'DeleteItems', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'confirmation', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}]}, 'CreateItemsInput': {'name': 'CreateItemsInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'ReadItemsInput': {'name': 'ReadItemsInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'UpdateItemsInput': {'name': 'UpdateItemsInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'DeleteItemsInput': {'name': 'DeleteItemsInput', 'kind': 'INPUT_OBJECT', 'ofType': None, 'fields': None}, 'RootItemField': {'name': 'RootItemField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'name', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'price', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Float', 'kind': 'SCALAR'}}}, {'name': 'clients', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, 'LeafItemField': {'name': 'LeafItemField', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'id', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'ID', 'kind': 'SCALAR'}}}, {'name': 'name', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'price', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Float', 'kind': 'SCALAR'}}}]}}
        self.assertEqual(executed["data"], expected)
    def testIntroseption(self):
        query = """{
            __Schema : __type(name: "__Schema") {...instrospection}
            __Type : __type(name: "__Type") {...instrospection}
            __TypeKind : __type(name: "__TypeKind") {...instrospection}
            __Field : __type(name: "__Field") {...instrospection}
            __InputValue : __type(name: "__InputValue") {...instrospection}
            __EnumValue : __type(name: "__EnumValue") {...instrospection}
            __Directive : __type(name: "__Directive") {...instrospection}
            __DirectiveLocation : __type(name: "__DirectiveLocation") {...instrospection}
        }
        fragment instrospection on __Type {
            name
            kind
            ofType {
                name
                kind
            }
            fields {
                name
                type {
                    name
                    kind
                    ofType {
                        name
                        kind
                    }
                }
            }
        }"""
        executed = client.execute(query)
        expected = {'__Schema': {'name': '__Schema', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'types', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}, {'name': 'queryType', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': '__Type', 'kind': 'OBJECT'}}}, {'name': 'mutationType', 'type': {'name': '__Type', 'kind': 'OBJECT', 'ofType': None}}, {'name': 'subscriptionType', 'type': {'name': '__Type', 'kind': 'OBJECT', 'ofType': None}}, {'name': 'directives', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, '__Type': {'name': '__Type', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'kind', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': '__TypeKind', 'kind': 'ENUM'}}}, {'name': 'name', 'type': {'name': 'String', 'kind': 'SCALAR', 'ofType': None}}, {'name': 'description', 'type': {'name': 'String', 'kind': 'SCALAR', 'ofType': None}}, {'name': 'fields', 'type': {'name': None, 'kind': 'LIST', 'ofType': {'name': None, 'kind': 'NON_NULL'}}}, {'name': 'interfaces', 'type': {'name': None, 'kind': 'LIST', 'ofType': {'name': None, 'kind': 'NON_NULL'}}}, {'name': 'possibleTypes', 'type': {'name': None, 'kind': 'LIST', 'ofType': {'name': None, 'kind': 'NON_NULL'}}}, {'name': 'enumValues', 'type': {'name': None, 'kind': 'LIST', 'ofType': {'name': None, 'kind': 'NON_NULL'}}}, {'name': 'inputFields', 'type': {'name': None, 'kind': 'LIST', 'ofType': {'name': None, 'kind': 'NON_NULL'}}}, {'name': 'ofType', 'type': {'name': '__Type', 'kind': 'OBJECT', 'ofType': None}}]}, '__TypeKind': {'name': '__TypeKind', 'kind': 'ENUM', 'ofType': None, 'fields': None}, '__Field': {'name': '__Field', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'name', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'description', 'type': {'name': 'String', 'kind': 'SCALAR', 'ofType': None}}, {'name': 'args', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}, {'name': 'type', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': '__Type', 'kind': 'OBJECT'}}}, {'name': 'isDeprecated', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}, {'name': 'deprecationReason', 'type': {'name': 'String', 'kind': 'SCALAR', 'ofType': None}}]}, '__InputValue': {'name': '__InputValue', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'name', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'description', 'type': {'name': 'String', 'kind': 'SCALAR', 'ofType': None}}, {'name': 'type', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': '__Type', 'kind': 'OBJECT'}}}, {'name': 'defaultValue', 'type': {'name': 'String', 'kind': 'SCALAR', 'ofType': None}}]}, '__EnumValue': {'name': '__EnumValue', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'name', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'description', 'type': {'name': 'String', 'kind': 'SCALAR', 'ofType': None}}, {'name': 'isDeprecated', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'Boolean', 'kind': 'SCALAR'}}}, {'name': 'deprecationReason', 'type': {'name': 'String', 'kind': 'SCALAR', 'ofType': None}}]}, '__Directive': {'name': '__Directive', 'kind': 'OBJECT', 'ofType': None, 'fields': [{'name': 'name', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': 'String', 'kind': 'SCALAR'}}}, {'name': 'description', 'type': {'name': 'String', 'kind': 'SCALAR', 'ofType': None}}, {'name': 'locations', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}, {'name': 'args', 'type': {'name': None, 'kind': 'NON_NULL', 'ofType': {'name': None, 'kind': 'LIST'}}}]}, '__DirectiveLocation': {'name': '__DirectiveLocation', 'kind': 'ENUM', 'ofType': None, 'fields': None}}
        self.assertEqual(executed["data"], expected)
    pass
pass
