#!/usr/bin/env python3
# coding=utf-8
# import
from unittest import TestCase
from test.graphQL import client
from time import sleep
from threading import Thread
from dummypythongraphqlserver.graphQL import schema, SubscriptionObserver
from dummypythongraphqlserver.entities import Address
# test class
class testAddress(TestCase):
    dynamicQuery = """query ($readAddressesInput:ReadAddressesInput!,$withClients:Boolean!,$withPurchaseOrders:Boolean!,$withOrderLines:Boolean!) {
        readAddresses (readAddressesInput:$readAddressesInput,withClients:$withClients,withPurchaseOrders:$withPurchaseOrders,withOrderLines:$withOrderLines) {
            houseNumber,
            street,
            zipCode,
            city,
            clients @include(if: $withClients) {
                firstName,
                lastName,
                purchaseOrders @include(if: $withPurchaseOrders) {
                    id,
                    creationDate,
                    orderLines @include(if: $withOrderLines) {
                        item {
                            name,
                            price
                        },
                        quantity
                    }
                }
            }
        }
    }"""
    def testAddress00(self):
        query = """{
            readAddresses (readAddressesInput:{street:"st."}) {
                id,
                houseNumber,
                street,
                zipCode,
                city
            }
        }"""
        executed = client.execute(query)
        expected = {'readAddresses': [{'id': '1', 'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee'}, {'id': '4', 'houseNumber': '7763', 'street': 'Bedford St.', 'zipCode': '29646', 'city': 'Greenwood'}, {'id': '12', 'houseNumber': '23', 'street': 'Ridgeview St.', 'zipCode': '80123', 'city': 'Littleton'}, {'id': '13', 'houseNumber': '938', 'street': 'Hanover St.', 'zipCode': '30134', 'city': 'Douglasville'}, {'id': '15', 'houseNumber': '7', 'street': 'High St.', 'zipCode': '7032', 'city': 'Kearny'}, {'id': '17', 'houseNumber': '81', 'street': 'Aspen St.', 'zipCode': '33823', 'city': 'Auburndale'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testAddress01(self):
        query = """{
            readAddresses (readAddressesInput:{street:"st.", city:"le"}) {
                houseNumber,
                street,
                zipCode,
                city
            }
        }"""
        executed = client.execute(query)
        expected = {'readAddresses': [{'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee'}, {'houseNumber': '23', 'street': 'Ridgeview St.', 'zipCode': '80123', 'city': 'Littleton'}, {'houseNumber': '938', 'street': 'Hanover St.', 'zipCode': '30134', 'city': 'Douglasville'}, {'houseNumber': '81', 'street': 'Aspen St.', 'zipCode': '33823', 'city': 'Auburndale'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testAddress02(self):
        query = """{
            readAddresses (readAddressesInput:{limit:3,offset:2}) {
                id
            }
        }"""
        executed = client.execute(query)
        expected = {'readAddresses': [{'id': '3'}, {'id': '4'}, {'id': '5'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testAddress03(self):
        variables = {"readAddressesInput": {"street": "st.", "city": "le"}, "withClients": True, "withPurchaseOrders": False, "withOrderLines": False}
        executed = client.execute(testAddress.dynamicQuery,variables=variables)
        expected = {'readAddresses': [{'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee', 'clients': [{'firstName': 'Stephen', 'lastName': 'King'}, {'firstName': 'Stephen', 'lastName': 'Hawking'}]}, {'houseNumber': '23', 'street': 'Ridgeview St.', 'zipCode': '80123', 'city': 'Littleton', 'clients': [{'firstName': 'Taylor', 'lastName': 'Swift'}]}, {'houseNumber': '938', 'street': 'Hanover St.', 'zipCode': '30134', 'city': 'Douglasville', 'clients': []}, {'houseNumber': '81', 'street': 'Aspen St.', 'zipCode': '33823', 'city': 'Auburndale', 'clients': []}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testAddress04(self):
        variables = {"readAddressesInput": {"street": "st.", "city": "le"}, "withClients": False, "withPurchaseOrders": False, "withOrderLines": False}
        executed = client.execute(testAddress.dynamicQuery,variables=variables)
        expected = {'readAddresses': [{'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee'}, {'houseNumber': '23', 'street': 'Ridgeview St.', 'zipCode': '80123', 'city': 'Littleton'}, {'houseNumber': '938', 'street': 'Hanover St.', 'zipCode': '30134', 'city': 'Douglasville'}, {'houseNumber': '81', 'street': 'Aspen St.', 'zipCode': '33823', 'city': 'Auburndale'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testAddress05(self):
        variables = {"readAddressesInput": {"street": "st."}, "withClients": True, "withPurchaseOrders": True, "withOrderLines": True}
        executed = client.execute(testAddress.dynamicQuery,variables=variables)
        expected = {'readAddresses': [{'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee', 'clients': [{'firstName': 'Stephen', 'lastName': 'King', 'purchaseOrders': [{'id': '16', 'creationDate': 20080828, 'orderLines': [{'item': {'name': 'pumpkin pie', 'price': 1.58}, 'quantity': 7}, {'item': {'name': 'pumpkin seeds', 'price': 3.15}, 'quantity': 77}, {'item': {'name': 'chicken (raw)', 'price': 8.4}, 'quantity': 81}]}]}, {'firstName': 'Stephen', 'lastName': 'Hawking', 'purchaseOrders': [{'id': '6', 'creationDate': 20110306, 'orderLines': [{'item': {'name': 'porkchop (cooked)', 'price': 13.65}, 'quantity': 69}]}]}]}, {'houseNumber': '7763', 'street': 'Bedford St.', 'zipCode': '29646', 'city': 'Greenwood', 'clients': [{'firstName': 'Kate', 'lastName': 'Upton', 'purchaseOrders': [{'id': '1', 'creationDate': 20040717, 'orderLines': [{'item': {'name': 'baked potato (cooked)', 'price': 52.5}, 'quantity': 1}, {'item': {'name': 'golden apple', 'price': 40.02}, 'quantity': 89}, {'item': {'name': 'melon (block)', 'price': 25.15}, 'quantity': 95}, {'item': {'name': 'beef/steak (cooked)', 'price': 11.55}, 'quantity': 98}, {'item': {'name': 'sugar cane', 'price': 2.1}, 'quantity': 2}, {'item': {'name': 'melon (slice)', 'price': 2.1}, 'quantity': 59}, {'item': {'name': 'beef/steak (raw)', 'price': 1.05}, 'quantity': 3}, {'item': {'name': 'milk', 'price': 4.5}, 'quantity': 82}]}]}]}, {'houseNumber': '23', 'street': 'Ridgeview St.', 'zipCode': '80123', 'city': 'Littleton', 'clients': [{'firstName': 'Taylor', 'lastName': 'Swift', 'purchaseOrders': []}]}, {'houseNumber': '938', 'street': 'Hanover St.', 'zipCode': '30134', 'city': 'Douglasville', 'clients': []}, {'houseNumber': '7', 'street': 'High St.', 'zipCode': '7032', 'city': 'Kearny', 'clients': []}, {'houseNumber': '81', 'street': 'Aspen St.', 'zipCode': '33823', 'city': 'Auburndale', 'clients': []}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testAddress06(self):
        variables = {"readAddressesInput": {"street": "st."}, "withClients": False, "withPurchaseOrders": False, "withOrderLines": True}
        executed = client.execute(testAddress.dynamicQuery,variables=variables)
        expected = {'readAddresses': [{'houseNumber': '765', 'street': 'Thomas St.', 'zipCode': '7024', 'city': 'Fort Lee'}, {'houseNumber': '7763', 'street': 'Bedford St.', 'zipCode': '29646', 'city': 'Greenwood'}, {'houseNumber': '23', 'street': 'Ridgeview St.', 'zipCode': '80123', 'city': 'Littleton'}, {'houseNumber': '938', 'street': 'Hanover St.', 'zipCode': '30134', 'city': 'Douglasville'}, {'houseNumber': '7', 'street': 'High St.', 'zipCode': '7032', 'city': 'Kearny'}, {'houseNumber': '81', 'street': 'Aspen St.', 'zipCode': '33823', 'city': 'Auburndale'}]}
        self.assertEqual(executed["data"], expected)
        pass
    def testAddress07(self):
        query = """{
            readAddresses (readAddressesInput:{street: 0}) {
                id
            }
        }"""
        executed = client.execute(query)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "readAddressesInput" has invalid value {street: 0}.\nIn field "street": Expected type "String", found 0.""")
        pass
    def testAddress08(self):
        query = """{
            readAddresses (addressesInput_:{street: "st."}) {
                id
            }
        }"""
        executed = client.execute(query)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "addressesInput_" on field "readAddresses" of type "Query". Did you mean "readAddressesInput"?""")
        pass
    def testAddress09(self):
        # clean
        deleteMutation = """mutation {
            deleteAddresses(deleteAddressesInput:{houseNumber:1, street:"STREET#", zipCode:10, city:"CITY#"}) {confirmation}
        }"""
        client.execute(deleteMutation)
        # create
        mutation = """mutation {
            createAddresses (createAddressesInput:[{houseNumber:10, street:"STREET#0", zipCode:100, city:"CITY#0"},{houseNumber:11, street:"STREET#1", zipCode:101, city:"CITY#1"}]) {
                readAddresses {id}
            }
        }"""
        executed = client.execute(mutation)
        ids=list()
        for address in executed["data"]["createAddresses"]["readAddresses"] :
            self.assertIsNotNone(address["id"])
            ids.append(address["id"])
        # update
        mutation = """mutation {
            updateAddresses (updateAddressesInput:[{id:"""+ids[0]+""", houseNumber:12, street:"STREET#2", zipCode:102, city:"CITY#2"},{id:"""+ids[1]+""", houseNumber:103, street:"STREET#3", zipCode:103, city:"CITY#3"}]) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertTrue(executed["data"]["updateAddresses"]["confirmation"])
        # delete
        executed = client.execute(deleteMutation)
        self.assertTrue(executed["data"]["deleteAddresses"]["confirmation"])
        pass
    def testAddress10(self):
        # clean
        deleteMutation = """mutation deleteMutation ($deleteAddressesInput:DeleteAddressesInput!) {
            deleteAddresses(deleteAddressesInput:$deleteAddressesInput) {confirmation}
        }"""
        deleteVariables = {"deleteAddressesInput": {"houseNumber":1, "street":"STREET#", "zipCode":10, "city":"CITY#"}}
        client.execute(deleteMutation,variables=deleteVariables)
        # create
        mutation = """mutation createMutation ($createAddressesInput:[CreateAddressesInput!]!) {
            createAddresses (createAddressesInput:$createAddressesInput) {
                readAddresses {id}
            }
        }"""
        variables = {"createAddressesInput": [{"houseNumber":10, "street":"STREET#0", "zipCode":100, "city":"CITY#0"},{"houseNumber":11, "street":"STREET#1", "zipCode":101, "city":"CITY#1"}]}
        executed = client.execute(mutation,variables=variables)
        ids=list()
        for address in executed["data"]["createAddresses"]["readAddresses"] :
            self.assertIsNotNone(address["id"])
            ids.append(address["id"])
        # update
        mutation = """mutation updateMutation ($updateAddressesInput:[UpdateAddressesInput!]!) {
            updateAddresses (updateAddressesInput:$updateAddressesInput) {confirmation}
        }"""
        variables = {"updateAddressesInput": [{"id":ids[0], "houseNumber":12, "street":"STREET#2", "zipCode":102, "city":"CITY#2"},{"id":ids[1], "houseNumber":13, "street":"STREET#3", "zipCode":103, "city":"CITY#3"}]}
        executed = client.execute(mutation,variables=variables)
        self.assertTrue(executed["data"]["updateAddresses"]["confirmation"])
        # delete
        executed = client.execute(deleteMutation,variables=deleteVariables)
        self.assertTrue(executed["data"]["deleteAddresses"]["confirmation"])
        pass
    def testAddress11(self):
        mutation = """mutation {
            deleteAddresses(deleteAddressesInput:{houseNumber:"a", street:"STREET#", zipCode:10, city:"CITY#"}) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "deleteAddressesInput" has invalid value {houseNumber: "a", street: "STREET#", zipCode: 10, city: "CITY#"}.\nIn field "houseNumber": Expected type "Int", found "a".""")
        pass
    def testAddress12(self):
        mutation = """mutation {
            deleteAddresses(deleteAddressesInput_:{houseNumber:1, street:"STREET#", zipCode:10, city:"CITY#"}) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "deleteAddressesInput_" on field "deleteAddresses" of type "Mutation". Did you mean "deleteAddressesInput"?""")
        pass
    def testAddress13(self):
        mutation = """mutation {
            createAddresses(createAddressesInput:[{houseNumber:"a", street:"STREET#0", zipCode:100, city:"CITY#0"}]) {readAddresses {id}}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "createAddressesInput" has invalid value [{houseNumber: "a", street: "STREET#0", zipCode: 100, city: "CITY#0"}].\nIn element #0: In field "houseNumber": Expected type "Int", found "a".""")
        pass
    def testAddress14(self):
        mutation = """mutation {
            createAddresses(createAddressesInput_:[{houseNumber:10, street:"STREET#0", zipCode:100, city:"CITY#0"}]) {readAddresses {id}}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "createAddressesInput_" on field "createAddresses" of type "Mutation". Did you mean "createAddressesInput"?""")
        pass
    pass
    def testAddress15(self):
        mutation = """mutation {
            updateAddresses(updateAddressesInput:[{id:1, houseNumber:"a", street:"STREET#2", zipCode:102, city:"CITY#2"}]) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Argument "updateAddressesInput" has invalid value [{id: 1, houseNumber: "a", street: "STREET#2", zipCode: 102, city: "CITY#2"}].\nIn element #0: In field "houseNumber": Expected type "Int", found "a".""")
        pass
    def testAddress16(self):
        mutation = """mutation {
            updateAddresses(updateAddressesInput_:[{id:1, houseNumber:12, street:"STREET#2", zipCode:102, city:"CITY#2"}]) {confirmation}
        }"""
        executed = client.execute(mutation)
        self.assertEqual(executed["errors"][0]["message"],"""Unknown argument "updateAddressesInput_" on field "updateAddresses" of type "Mutation". Did you mean "updateAddressesInput"?""")
        pass
    def testAddress17(self):
        # subscribe
        subscription = """subscription ($readAddressesInput:ReadAddressesInput!,$period:Int!,$occurencesNumber:Int!,$withClients:Boolean!,$withPurchaseOrders:Boolean!,$withOrderLines:Boolean!) {
            readAddresses (readAddressesInput:$readAddressesInput,period:$period,occurencesNumber:$occurencesNumber,withClients:$withClients,withPurchaseOrders:$withPurchaseOrders,withOrderLines:$withOrderLines) {
                houseNumber,
                street,
                zipCode,
                city,
                clients @include(if: $withClients) {
                    firstName,
                    lastName,
                    purchaseOrders @include(if: $withPurchaseOrders) {
                        id,
                        creationDate,
                        orderLines @include(if: $withOrderLines) {
                            item {
                                name,
                                price
                            },
                            quantity
                        }
                    }
                }
            }
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        variables = {"readAddressesInput": {"houseNumber":1, "street":"STREET#", "zipCode":10, "city":"CITY#"}, "period":1, "occurencesNumber":20, "withClients": True, "withPurchaseOrders": True, "withOrderLines": True}
        # run observer & mutation thread
        observer = TestAddressesObserver(subscription, variables)
        TestAddressesUpdateThread(int(observer.period*observer.occurencesNumber/4)).start()
        observable = schema.execute(subscription,variables=variables,allow_subscriptions=True)
        # INFO : timestamp decorator function increment a loop conter name 'value'
        observable.interval(observer.period*1000).timestamp().filter(lambda self : self.value < observer.occurencesNumber).subscribe(observer=observer)
        sleep(observer.period*(observer.occurencesNumber+1))
        # check results
        self.assertEqual(len(observer.results), 3)
        self.assertIn(tuple(), observer.results)
        pass
    def testAddress18(self):
        # subscribe
        subscription = """subscription ($readAddressesInput:ReadAddressesInput!,$period:Int!,$occurencesNumber:Int!,$withClients:Boolean!,$withPurchaseOrders:Boolean!,$withOrderLines:Boolean!) {
            readAddresses (readAddressesInput:$readAddressesInput,period:$period,occurencesNumber:$occurencesNumber,withClients:$withClients,withPurchaseOrders:$withPurchaseOrders,withOrderLines:$withOrderLines) {id}
        }"""
        '''INFO :
        the client.execute method return a ordered dict, not an observable object,
        so we test subscription without test client
        '''
        variables = {"readAddressesInput": {"houseNumber":"a", "street":"STREET#", "zipCode":10, "city":"CITY#"}, "period":1, "occurencesNumber":1, "withClients": True, "withPurchaseOrders": True, "withOrderLines": True}
        # run observable
        observable = schema.execute(subscription,variables=variables,allow_subscriptions=True)
        # check results
        self.assertEqual(str(observable.errors[0]),"""could not convert string to float: 'a'""")
        pass
    pass
# test subscription
class TestAddressesObserver(SubscriptionObserver):
    # methods
    @staticmethod
    def instanciate(result):
        instance = Address(houseNumber=result.get("houseNumber") ,street=result.get("street") ,zipCode=result.get("zipCode") ,city=result.get("city") ,id=result.get("id"))
        return instance
    # inheritance
    def on_next(self, value):
        result = super().on_next(value)
        instances = list()
        for element in result.data["readAddresses"]:
            instance = TestAddressesObserver.instanciate(element)
            instances.append(instance)
            pass
        self.results.add(tuple(instances))
        #print("DEBUG : AddressesObserver.on_next")
    # constructor
    def __init__(self, subscription,variables):
        super().__init__(subscription,variables)
        self.results = set()
    pass
class TestAddressesUpdateThread(Thread):
    # inheritance
    def run(self):
        # clean
        deleteMutation = """mutation {
            deleteAddresses(deleteAddressesInput:{houseNumber:1, street:"STREET#", zipCode:10, city:"CITY#"}) {confirmation}
        }"""
        client.execute(deleteMutation)
        #print("DEBUG : CLEAN")
        sleep(self.waitTime)
        # create
        mutation = """mutation createMutation ($createAddressesInput:[CreateAddressesInput!]!) {
            createAddresses (createAddressesInput:$createAddressesInput) {
                readAddresses {id}
            }
        }"""
        variables = {"createAddressesInput": [{"houseNumber":10, "street":"STREET#0", "zipCode":100, "city":"CITY#0"},{"houseNumber":11, "street":"STREET#1", "zipCode":101, "city":"CITY#1"}]}
        executed = client.execute(mutation,variables=variables)
        ids=list()
        for address in executed["data"]["createAddresses"]["readAddresses"] :
            ids.append(address["id"])
        #print("DEBUG : INSERT")
        sleep(self.waitTime)
        # update
        mutation = """mutation updateMutation ($updateAddressesInput:[UpdateAddressesInput!]!) {
            updateAddresses (updateAddressesInput:$updateAddressesInput) {confirmation}
        }"""
        variables = {"updateAddressesInput": [{"id":ids[0], "houseNumber":12, "street":"STREET#2", "zipCode":102, "city":"CITY#2"},{"id":ids[1], "houseNumber":13, "street":"STREET#3", "zipCode":103, "city":"CITY#3"}]}
        client.execute(mutation,variables=variables)
        #print("DEBUG : UPDATE")
        sleep(self.waitTime)
        # clean
        client.execute(deleteMutation)
        #print("DEBUG : CLEAN")
        pass
    # constructor
    def __init__(self, waitTime):
        super().__init__()
        self.waitTime = waitTime
    pass
pass
