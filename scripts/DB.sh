#!/usr/bin/env bash
# coding=utf-8
# work in script path
ORIGINALPATH=`pwd`
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
cd ${SCRIPTPATH}
# clean DB
rm -f ../dummy.db
# load definition
sqlite3 ../dummy.db < DDL.sql
# load manipulation
sqlite3 ../dummy.db < DML.sql
# back to original path
cd ${ORIGINALPATH}
