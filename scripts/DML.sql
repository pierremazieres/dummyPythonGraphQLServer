-- clean DB
DELETE FROM ORDER_LINE;
DELETE FROM PURCHASE_ORDER;
DELETE FROM CLIENT;
DELETE FROM ADDRESS;
DELETE FROM ITEM;
-- fill DB
INSERT INTO ITEM (NAME, PRICE)
VALUES ('chicken (raw)', 8.4),
('chicken (cooked)', 9.45),
('porkchop (raw)', 12.6),
('porkchop (cooked)', 13.65),
('beef/steak (raw)', 1.05),
('beef/steak (cooked)', 11.55),
('fish (raw)', 20.9),
('fish (cooked)', 21.95),
('potato (raw)', 42.0),
('baked potato (cooked)', 52.5),
('poisonous potato', 1.5),
('carrot', 4.2),
('red apple', 60.36),
('golden apple', 40.02),
('melon (slice)', 2.1),
('melon (block)', 25.15),
('pumpkin', 10.6),
('brown mushroom', 6.3),
('red mushroom', 6.3),
('mushroom stew', 15),
('bread', 5.3),
('cookie', 1.6),
('pumpkin pie', 1.58),
('cake', 30),
('seeds', 2.1),
('melon seeds', 2.1),
('pumpkin seeds', 3.15),
('cocoa beans', 2.1),
('wheat', 2.1),
('sugar cane', 2.1),
('sugar', 2.1),
('egg', 2.5),
('milk', 4.5),
('hay bale', 2.09),
('glistering melon', 4.02)
;
INSERT INTO ADDRESS (HOUSE_NUMBER, STREET, CITY, ZIPCODE)
VALUES (765, 'Thomas St.', 'Fort Lee', 7024),
(9280, 'Water Court', 'Maryville', 37803),
(8024, 'Cleveland Street', 'Maplewood', 7040),
(7763, 'Bedford St.', 'Greenwood', 29646),
(3, 'Jockey Hollow Dr.', 'Bridgeport', 6606),
(896, 'Jefferson Ave.', 'Findlay', 45840),
(42, 'Annadale Lane', 'Morganton', 28655),
(895, 'West Pheasant Lane', 'Asbury Park', 7712),
(5, 'Linda Street', 'Euless', 76039),
(441, 'Buckingham Drive', 'Lady Lake', 32159),
(935, 'Pineknoll Street', 'Quakertown', 18951),
(23, 'Ridgeview St.', 'Littleton', 80123),
(938, 'Hanover St.', 'Douglasville', 30134),
(874, 'Valley Lane', 'Dubuque', 52001),
(7, 'High St.', 'Kearny', 07032),
(9740, 'Armstrong Dr.', 'Sykesville', 21784),
(81, 'Aspen St.', 'Auburndale', 33823),
(8464, 'Trusel Street', 'Painesville', 44077),
(563, 'Main Dr.', 'Pueblo', 81001),
(7169, 'Oakland Road', 'Norcross', 30092)
;
INSERT INTO CLIENT (FIRST_NAME, LAST_NAME, ADDRESS_ID)
VALUES ('Stephen', 'King', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 0)),
('Dave', 'Matthews', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 1)),
('John', 'Bon Jovi', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 2)),
('Stephen', 'Hawking', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 0)),
('Kate', 'Upton', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 3)),
('Frank', 'Zappa', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 4)),
('Raoul', 'Wallenberg', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 5)),
('Anne', 'Hathaway', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 4)),
('Ben', 'Stiller', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 6)),
('Charles', 'Darwin', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 4)),
('Michael', 'Jordan', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 7)),
('Toby', 'Keith', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 8)),
('Scarlett', 'Johansson', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 9)),
('Rush', 'Limbaugh', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 10)),
('Taylor', 'Swift', (SELECT ID FROM ADDRESS LIMIT 1 OFFSET 11))
;
INSERT INTO PURCHASE_ORDER (CLIENT_ID , CREATION_DATE)
VALUES ((SELECT ID FROM CLIENT LIMIT 1 OFFSET 6), 20040717),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 3), 20030617),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 4), 20070308),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 3), 20020703),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 2), 20060607),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 11), 20110306),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 4), 20121227),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 1), 20120118),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 14), 20061215),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 0), 20120622),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 9), 20150125),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 10), 20011119),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 14), 20151210),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 14), 20080307),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 3), 20060422),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 12), 20080828),
((SELECT ID FROM CLIENT LIMIT 1 OFFSET 9), 20080324)
;
INSERT INTO ORDER_LINE (ORDER_ID, ITEM_ID, QUANTITY)
VALUES ((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 0), (SELECT ID FROM ITEM LIMIT 1 OFFSET 0), 1),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 8), (SELECT ID FROM ITEM LIMIT 1 OFFSET 14), 41),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 8), (SELECT ID FROM ITEM LIMIT 1 OFFSET 15), 41),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 2), (SELECT ID FROM ITEM LIMIT 1 OFFSET 8), 38),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 4), (SELECT ID FROM ITEM LIMIT 1 OFFSET 8), 9),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 8), (SELECT ID FROM ITEM LIMIT 1 OFFSET 12), 56),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 4), (SELECT ID FROM ITEM LIMIT 1 OFFSET 1), 13),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 9), (SELECT ID FROM ITEM LIMIT 1 OFFSET 17), 53),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 12), (SELECT ID FROM ITEM LIMIT 1 OFFSET 16), 2),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 1), (SELECT ID FROM ITEM LIMIT 1 OFFSET 3), 18),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 15), (SELECT ID FROM ITEM LIMIT 1 OFFSET 27), 7),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 15), (SELECT ID FROM ITEM LIMIT 1 OFFSET 28), 77),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 14), (SELECT ID FROM ITEM LIMIT 1 OFFSET 13), 80),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 14), (SELECT ID FROM ITEM LIMIT 1 OFFSET 34), 28),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 0), (SELECT ID FROM ITEM LIMIT 1 OFFSET 15), 89),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 11), (SELECT ID FROM ITEM LIMIT 1 OFFSET 5), 67),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 8), (SELECT ID FROM ITEM LIMIT 1 OFFSET 7), 68),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 4), (SELECT ID FROM ITEM LIMIT 1 OFFSET 9), 18),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 0), (SELECT ID FROM ITEM LIMIT 1 OFFSET 17), 95),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 5), (SELECT ID FROM ITEM LIMIT 1 OFFSET 23), 69),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 16), (SELECT ID FROM ITEM LIMIT 1 OFFSET 15), 97),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 2), (SELECT ID FROM ITEM LIMIT 1 OFFSET 14), 97),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 8), (SELECT ID FROM ITEM LIMIT 1 OFFSET 10), 44),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 8), (SELECT ID FROM ITEM LIMIT 1 OFFSET 19), 92),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 1), (SELECT ID FROM ITEM LIMIT 1 OFFSET 7), 17),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 0), (SELECT ID FROM ITEM LIMIT 1 OFFSET 1), 98),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 11), (SELECT ID FROM ITEM LIMIT 1 OFFSET 31), 92),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 9), (SELECT ID FROM ITEM LIMIT 1 OFFSET 2), 85),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 11), (SELECT ID FROM ITEM LIMIT 1 OFFSET 25), 29),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 0), (SELECT ID FROM ITEM LIMIT 1 OFFSET 33), 2),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 0), (SELECT ID FROM ITEM LIMIT 1 OFFSET 18), 59),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 15), (SELECT ID FROM ITEM LIMIT 1 OFFSET 8), 81),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 0), (SELECT ID FROM ITEM LIMIT 1 OFFSET 2), 3),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 11), (SELECT ID FROM ITEM LIMIT 1 OFFSET 10), 52),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 13), (SELECT ID FROM ITEM LIMIT 1 OFFSET 16), 26),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 13), (SELECT ID FROM ITEM LIMIT 1 OFFSET 10), 83),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 7), (SELECT ID FROM ITEM LIMIT 1 OFFSET 17), 39),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 0), (SELECT ID FROM ITEM LIMIT 1 OFFSET 20), 82),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 9), (SELECT ID FROM ITEM LIMIT 1 OFFSET 10), 65),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 8), (SELECT ID FROM ITEM LIMIT 1 OFFSET 26), 30),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 14), (SELECT ID FROM ITEM LIMIT 1 OFFSET 33), 49),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 7), (SELECT ID FROM ITEM LIMIT 1 OFFSET 8), 4),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 13), (SELECT ID FROM ITEM LIMIT 1 OFFSET 0), 11),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 2), (SELECT ID FROM ITEM LIMIT 1 OFFSET 15), 1),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 2), (SELECT ID FROM ITEM LIMIT 1 OFFSET 29), 60),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 6), (SELECT ID FROM ITEM LIMIT 1 OFFSET 34), 64),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 13), (SELECT ID FROM ITEM LIMIT 1 OFFSET 23), 65),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 6), (SELECT ID FROM ITEM LIMIT 1 OFFSET 4), 75),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 12), (SELECT ID FROM ITEM LIMIT 1 OFFSET 8), 47),
((SELECT ID FROM PURCHASE_ORDER LIMIT 1 OFFSET 9), (SELECT ID FROM ITEM LIMIT 1 OFFSET 13), 54)
;
