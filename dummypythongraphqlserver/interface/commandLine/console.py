#!/usr/bin/env python3
# coding=utf-8
# import
from dummypythongraphqlserver.graphQL import schema, SubscriptionObserver
from json import dumps, loads
from sys import exit
from time import sleep
from threading import Thread
from multiprocessing import Queue
# format result
def formatResponse(response):
    formattedResponse = "\n<<< data\n" + dumps(response.data, indent=4)
    if response.errors and len(response.errors) > 0:
        for index, error in enumerate(response.errors):
            if hasattr(error, "message"):
                error = error.message
                formattedResponse += "\n<<< error #" + str(index) + "\n" + str(error)
    return formattedResponse
# (thread safe) file writer
# INFO : for complete safety, this class should be a singleton
class FileWriter(Thread):
    queue = Queue()
    fileName = "out.txt"
    # inheritance
    def run(self):
        while True :
            response = FileWriter.queue.get()
            file = open(FileWriter.fileName, 'a')
            file.write(formatResponse(response))
            file.close()
    pass
# subscription
class SubscriptionThread(Thread):
    # inner observer
    class InnerObserver(SubscriptionObserver):
        # inheritance
        def on_next(self, value):
            response = super().on_next(value)
            FileWriter.queue.put(response)
    # inheritance
    def run(self):
        observer = SubscriptionThread.InnerObserver(self.request, self.parameters)
        observable = schema.execute(self.request, variables=self.parameters, allow_subscriptions=True)
        # INFO : timestamp decorator function increment a loop conter name 'value'
        observable.interval(observer.period * 1000).timestamp().filter( lambda o: o.value < observer.occurencesNumber).subscribe(observer=observer)
        sleep(observer.period * (observer.occurencesNumber + 1))
    # constructor
    def __init__(self, request, parameters):
        super().__init__()
        self.request = request
        self.parameters = parameters
# multiligne input
# WARN : this method do not allow you to edit previous line, only the current one
def multilineText():
    lines = []
    while True:
        line = input()
        if line:
            lines.append(line)
        else:
            break
    text = '\n'.join(lines)
    return text
# run console
def main():
    fileWriter = FileWriter()
    fileWriter.start()
    while True:
        try:
            # stop console message
            print("* press CTRL-C or CTRL-D to stop this console")
            # ask for request
            print(">>> request")
            request = multilineText()
            firstWord = request.split(maxsplit=1)[0]
            print(">>> parameters")
            parameters = multilineText()
            if len(parameters.strip()) > 0:
                parameters = loads(parameters)
            # asynchronous subscription
            if firstWord.upper() == "SUBSCRIPTION":
                SubscriptionThread(request, parameters).start()
            # synchronous query/mutation
            else:
                response = schema.execute(request, variables=parameters)
                print(formatResponse(response))
        except Exception as e:
            # keyboard interruption
            if str(e) == "EOF when reading a line":
                fileWriter._tstate_lock = None
                fileWriter._stop()
                exit()
            # print any other issue & continue looping
            else:
                print(e)
        pass
    pass
if __name__ == "__main__":
    main()
pass
