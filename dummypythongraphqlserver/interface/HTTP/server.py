#!/usr/bin/env python3
# coding=utf-8
# import
from threading import Thread
from http.server import BaseHTTPRequestHandler, HTTPServer
from http.client import HTTPConnection
from urllib.parse import parse_qs, urlsplit
from json import dumps, loads
from time import sleep
from dummypythongraphqlserver.graphQL import schema, SubscriptionObserver
# format response into JSON
def formatJsonResponse(rawResponse):
    jsonResponse = dict()
    if rawResponse.data:
        jsonResponse["data"] = rawResponse.data
    if rawResponse.errors and len(rawResponse.errors) > 0:
        jsonResponse["errors"] = list()
        for error in rawResponse.errors:
            if hasattr(error, "message"):
                error = error.message
                jsonResponse["errors"].append(error)
    return dumps(jsonResponse)
# subscription
class SubscriptionThread(Thread):
    # inner observer
    class InnerObserver(SubscriptionObserver):
        # inheritance
        def on_next(self, value):
            rawResponse = schema.execute(self.query, variables=self.variables)
            jsonResponse = formatJsonResponse(rawResponse)
            self.callbackConnection.request("POST", self.callbackPath, jsonResponse)
            # WARN : always acknowledge the response to unlock socket
            self.callbackConnection.getresponse()
            pass
        # constructor
        def __init__(self, request, parameters):
            super().__init__(request, parameters)
            # set callback connection
            callbackEndpoint = urlsplit(parameters["callbackEndpoint"])
            callbackHost, callbackPort = (callbackEndpoint[1]).split(':')
            self.callbackConnection = HTTPConnection(callbackHost, callbackPort)
            self.callbackPath = callbackEndpoint[2]
            pass
    # inheritance
    def run(self):
        observable = schema.execute(self.request, variables=self.parameters, allow_subscriptions=True)
        # check if query/variable error
        # INFO : on query/variable error, we get an standard JSON response, not an observable
        observer = self.observer
        if hasattr(observable,"errors"):
            jsonResponse = formatJsonResponse(observable)
            observer.callbackConnection.request("POST", observer.callbackPath, jsonResponse)
            # WARN : always acknowledge the response to unlock socket
            observer.callbackConnection.getresponse()
        else :
            # INFO : timestamp decorator function increment a loop counter name 'value'
            observable.interval(observer.period * 1000).timestamp().filter( lambda o: o.value < observer.occurencesNumber).subscribe(observer=observer)
            # wait expected time before shuting down thread
            sleep(observer.period * (observer.occurencesNumber + 1))
            observer.callbackConnection.close()
        pass
    # constructor
    def __init__(self, request, parameters):
        super().__init__()
        self.request = request
        self.parameters = parameters
        # set callback connection
        self.observer = SubscriptionThread.InnerObserver(self.request, self.parameters)
        pass
# HTTP server
# INFO : HTTP specification are resumed here : https://graphql.org/learn/serving-over-http
class HTTPRequestHandler(BaseHTTPRequestHandler):
    # INFO : in a real application, those fields should be dynamically parametrised
    host = '0.0.0.0'
    port = 8081
    expectedPath = "/graphql"
    # intercept GET request
    def do_GET(self):
        # specific input parser
        # INFO : GET request is like : http://[server]/[path]?query=[query]&variables=[variables]
        def getParser(self):
            query = urlsplit(self.path)[3]
            parameters = parse_qs(query)
            request = parameters.get("query")[0] # text
            variables = loads(parameters.get("variables")[0]) # JSON/dict
            return request, variables
        # do execution
        self.execute(getParser)
    # intercept POST request
    def do_POST(self):
        # specific input parser
        # INFO : POST body is like : {'query':[query],'variables':[variables]}
        def postParser(self):
            bodyLength = int(self.headers.get("content-length"))
            parameters = loads(self.rfile.read(bodyLength).decode())
            request = parameters.get("query") # text
            variables = parameters.get("variables") # JSON/dict
            return request, variables
        # do execution
        self.execute(postParser)
    # execute action
    def execute(self,specificInputParser):
        message = None
        try :
            # parse & check path
            path = urlsplit(self.path)[2]
            if path.upper() != HTTPRequestHandler.expectedPath.upper():
                raise Exception("Expected path : " + HTTPRequestHandler.expectedPath)
            # parse input
            request, variables = specificInputParser(self)
            # asynchronous subscription
            firstWord = request.split(maxsplit=1)[0]
            if firstWord.upper() == "SUBSCRIPTION":
                # check if call back exists
                if "callbackEndpoint" not in variables:
                    raise Exception("Please define an callback endpoint")
                try:
                    callbackEndpoint = urlsplit(variables["callbackEndpoint"])
                    callbackHost, callbackPort = (callbackEndpoint[1]).split(':')
                    callbackConnection = HTTPConnection(callbackHost, callbackPort)
                    callbackPath = callbackEndpoint[2]
                    # INFO : here the endpoint must respond
                    callbackConnection.request("POST", callbackPath)
                    # WARN : always acknowledge the response to unlock socket
                    callbackConnection.getresponse()
                except Exception as exeception:
                    # manage error
                    raise Exception("Your callbackendpoint ("+variables["callbackEndpoint"]+") is not responding")
                # run subscription
                SubscriptionThread(request, variables).start()
                message = dumps({"data" : "Check the callback endpoint for subscription result"})
            # synchronous query/mutation
            else:
                # execute query & format response
                rawResponse = schema.execute(request, variables=variables)
                message = formatJsonResponse(rawResponse)
        except Exception as exeception:
            # manage error
            message = dumps({"errors": [str(exeception)]})
        finally:
            # finalize response
            '''INFO : server will always send a technically fine response
            if any error occurs, it will be set in errors array'''
            self.send_response(200)  # OK
            self.send_header('Content-type', 'text/json')
            self.end_headers()
            self.wfile.write(bytes(message, "utf8"))
        pass
    pass
# run server
def run():
    print("starting server...")
    # INFO : adapt server settings if need
    server_address = (HTTPRequestHandler.host, HTTPRequestHandler.port)
    httpd = HTTPServer(server_address, HTTPRequestHandler)
    print("running server...\n")
    httpd.serve_forever()
if __name__ == "__main__":
    run()
pass
