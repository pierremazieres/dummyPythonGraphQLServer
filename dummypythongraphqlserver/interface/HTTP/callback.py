#!/usr/bin/env python3
# coding=utf-8
# import
from http.server import BaseHTTPRequestHandler, HTTPServer
from time import localtime, asctime
from json import dumps, loads
# callback stub
class HTTPCallbackStub(BaseHTTPRequestHandler):
    # INFO : in a real application, those fields should be dynamically parametrised
    host = '0.0.0.0'
    port = 8082
    expectedPath = "/dummyCallback"
    def do_POST(self):
        self.send_response(200)  # OK
        self.end_headers()
        bodyLength = int(self.headers.get("content-length"))
        content = dumps(loads(self.rfile.read(bodyLength).decode()), indent=4)
        print(asctime(localtime()) + '\n' + content)
    pass
# run callback stub
def run():
    print('starting callback stub...')
    # INFO : adapt server settings if need
    server_address = (HTTPCallbackStub.host, HTTPCallbackStub.port)
    httpd = HTTPServer(server_address, HTTPCallbackStub)
    print('running callback stub...')
    httpd.serve_forever()
if __name__ == "__main__":
    run()
pass
