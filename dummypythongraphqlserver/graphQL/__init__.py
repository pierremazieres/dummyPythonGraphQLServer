# coding=utf-8
# import
from re import sub
from rx import Observer
from rx.core.blockingobservable import BlockingObservable
from graphene import Schema, ObjectType, Field, NonNull, List, Boolean, Int, String
from dummypythongraphqlserver.graphQL.leaf.client import LeafClientField
from dummypythongraphqlserver.graphQL.leaf.address import LeafAddressField
from dummypythongraphqlserver.graphQL.leaf.purchaseOrder import LeafPurchaseOrderField
from dummypythongraphqlserver.graphQL.leaf.item import LeafItemField
from dummypythongraphqlserver.graphQL.root.client import RootClientField, ReadClientsInput, DeleteClients, CreateClients, UpdateClients
from dummypythongraphqlserver.graphQL.root.address import RootAddressField, ReadAddressesInput, DeleteAddresses, CreateAddresses, UpdateAddresses
from dummypythongraphqlserver.graphQL.root.purchaseOrder import RootPurchaseOrderField, ReadPurchaseOrdersInput, DeletePurchaseOrders, CreatePurchaseOrders, UpdatePurchaseOrders
from dummypythongraphqlserver.graphQL.root.item import RootItemField, ReadItemsInput, DeleteItems, CreateItems, UpdateItems
# query
class Query(ObjectType):
    readClients = Field(List(NonNull(RootClientField)),required=True, readClientsInput=ReadClientsInput(required=True), withAddress=Boolean(), withPurchaseOrders=Boolean(), withOrderLines=Boolean())
    readAddresses = Field(List(NonNull(RootAddressField)),required=True, readAddressesInput=ReadAddressesInput(required=True), withClients=Boolean(), withPurchaseOrders=Boolean(), withOrderLines=Boolean())
    readPurchaseOrders = Field(List(NonNull(RootPurchaseOrderField)),required=True, readPurchaseOrdersInput=ReadPurchaseOrdersInput(required=True), withOrderLines=Boolean(), withClient=Boolean(), withAddress=Boolean())
    readItems = Field(List(NonNull(RootItemField)),required=True, readItemsInput=ReadItemsInput(required=True), withClients=Boolean(), withAddress=Boolean(), withPurchaseOrders=Boolean())
    '''INFO : keep all parameters, even the ones you do not use later
    they will be used by graphen framework
    i.e : withAddress, withClient(s), withPurchaseOrders, withOrderLines'''
    def resolve_readClients(self, _, readClientsInput, withAddress=True, withPurchaseOrders=True, withOrderLines=True):
        leafClientFields = LeafClientField.resolveClients(ids=readClientsInput.ids, firstName=readClientsInput.firstName, lastName=readClientsInput.lastName, limit=readClientsInput.limit, offset=readClientsInput.offset)
        rootClientFields = list()
        for leafClientField in leafClientFields:
            rootClientField = RootItemField.ClientItemField(id=leafClientField.id, firstName=leafClientField.firstName, lastName=leafClientField.lastName)
            rootClientFields.append(rootClientField)
        return rootClientFields
    def resolve_readAddresses(self, _, readAddressesInput, withClients=True, withPurchaseOrders=True, withOrderLines=True):
        leafAddressFields = LeafAddressField.resolveAddresses(ids=readAddressesInput.ids, houseNumber=readAddressesInput.houseNumber, street=readAddressesInput.street, zipCode=readAddressesInput.zipCode, city=readAddressesInput.city, limit=readAddressesInput.limit, offset=readAddressesInput.offset)
        rootAddressFields = list()
        for leafAddressField in leafAddressFields:
            rootAddressField = RootAddressField(id=leafAddressField.id, houseNumber=leafAddressField.houseNumber, street=leafAddressField.street, zipCode=leafAddressField.zipCode, city=leafAddressField.city)
            rootAddressFields.append(rootAddressField)
        return rootAddressFields
    def resolve_readPurchaseOrders(self, _, readPurchaseOrdersInput, withOrderLines=True, withClient=True, withAddress=True):
        leafPurchaseOrderFields = LeafPurchaseOrderField.resolvePurchaseOrders(ids=readPurchaseOrdersInput.ids, minimumCreationDate=readPurchaseOrdersInput.minimumCreationDate, maximumCreationDate=readPurchaseOrdersInput.maximumCreationDate, limit=readPurchaseOrdersInput.limit, offset=readPurchaseOrdersInput.offset)
        rootPurchaseOrderFields = list()
        for leafPurchaseOrderField in leafPurchaseOrderFields:
            rootPurchaseOrderField = RootItemField.ClientItemField.PurchaseOrderClientField( id=leafPurchaseOrderField.id, creationDate=leafPurchaseOrderField.creationDate)
            rootPurchaseOrderFields.append(rootPurchaseOrderField)
        return rootPurchaseOrderFields
    def resolve_readItems(self, _, readItemsInput, withClients=True, withAddress=True, withPurchaseOrders=True):
        leafItemFields = LeafItemField.resolveItems(ids=readItemsInput.ids, name=readItemsInput.name, price=readItemsInput.price, limit=readItemsInput.limit, offset=readItemsInput.offset)
        rootItemFields = list()
        for leafItemField in leafItemFields:
            rootItemField = RootItemField(id=leafItemField.id, name=leafItemField.name, price=leafItemField.price)
            rootItemFields.append(rootItemField)
        return rootItemFields
    pass
# mutation
class Mutation(ObjectType):
    deleteClients = DeleteClients.Field()
    createClients = CreateClients.Field()
    updateClients = UpdateClients.Field()
    deleteAddresses = DeleteAddresses.Field()
    createAddresses = CreateAddresses.Field()
    updateAddresses = UpdateAddresses.Field()
    deletePurchaseOrders = DeletePurchaseOrders.Field()
    createPurchaseOrders = CreatePurchaseOrders.Field()
    updatePurchaseOrders = UpdatePurchaseOrders.Field()
    deleteItems = DeleteItems.Field()
    createItems = CreateItems.Field()
    updateItems = UpdateItems.Field()
# subscription
class SubscriptionObserver(Observer):
    # inheritance
    def on_next(self, value):
        result = schema.execute(self.query, variables=self.variables)
        return result
    def on_completed(self, value):
        return
    def on_error(self, value):
        return
    # constructor
    def __init__(self, subscription,variables):
        # INFO : a subscription is just a query repeated in time
        self.query = subscription.replace("subscription", "query")
        # INFO : remove useless parameters for query (dirty but efficient)
        self.query = sub(",(\s?)+\$period(\s?)+:(\s?)+Int!",'',self.query) # $period:Int!
        self.query = sub(",(\s?)+\$occurencesNumber(\s?)+:(\s?)+Int!",'',self.query) # $occurencesNumber:Int!
        self.query = sub(",(\s?)+\$callbackEndpoint(\s?)+:(\s?)+String",'',self.query) # $callbackEndpoint:String
        self.query = sub(",(\s?)+period(\s?)+:(\s?)+\$period", '', self.query) # period:$period
        self.query = sub(",(\s?)+occurencesNumber(\s?)+:(\s?)+\$occurencesNumber", '', self.query) # occurencesNumber:$occurencesNumber
        self.query = sub(",(\s?)+callbackEndpoint(\s?)+:(\s?)+\$callbackEndpoint", '', self.query) # callbackEndpoint:$callbackEndpoint
        self.period = variables.get("period")
        self.occurencesNumber = variables.get("occurencesNumber")
        self.callbackEndpoint = variables.get("callbackEndpoint")
        # INFO : do a copy to separate subscription & query parameters
        self.variables = dict(variables)
        del self.variables["period"]
        del self.variables["occurencesNumber"]
        if "callbackEndpoint" in self.variables:
            del self.variables["callbackEndpoint"]
        pass
    pass
class Subscription(ObjectType):
    # INFO : input parameters are almost the same than queries one, except we add period (in second) and occurences number
    readClients = Field(List(NonNull(RootClientField)),required=True, readClientsInput=ReadClientsInput(required=True), period=Int(required=True), occurencesNumber=Int(required=True), callbackEndpoint=String(), withAddress=Boolean(), withPurchaseOrders=Boolean(), withOrderLines=Boolean())
    readAddresses = Field(List(NonNull(RootAddressField)),required=True, readAddressesInput=ReadAddressesInput(required=True), period=Int(required=True), occurencesNumber=Int(required=True), callbackEndpoint=String(), withClients=Boolean(), withPurchaseOrders=Boolean(), withOrderLines=Boolean())
    readPurchaseOrders = Field(List(NonNull(RootPurchaseOrderField)),required=True, readPurchaseOrdersInput=ReadPurchaseOrdersInput(required=True), period=Int(required=True), occurencesNumber=Int(required=True), callbackEndpoint=String(), withOrderLines=Boolean(), withClient=Boolean(), withAddress=Boolean())
    readItems = Field(List(NonNull(RootItemField)),required=True, readItemsInput=ReadItemsInput(required=True), period=Int(required=True), occurencesNumber=Int(required=True), callbackEndpoint=String(), withClients=Boolean(), withAddress=Boolean(), withPurchaseOrders=Boolean())
    '''
    WARN : There is many reason we return a default observABLE object,
    all due to the fact RxPy framework overload our observable object to an anonymous one:
     - custom attributs are lost
     - the subcribe method is embedded in others one, witch are difficult to unpile
    Therefore, all algorithm will be implmented in observER object
    '''
    def resolve_readClients(self, _, readClientsInput, period, occurencesNumber, callbackEndpoint=None, withAddress=True, withPurchaseOrders=True, withOrderLines=True):
        return BlockingObservable()
    def resolve_readAddresses(self, _, readAddressesInput, period, occurencesNumber, callbackEndpoint=None, withClients=True, withPurchaseOrders=True, withOrderLines=True):
        return BlockingObservable()
    def resolve_readPurchaseOrders(self, _, readPurchaseOrdersInput, period, occurencesNumber, callbackEndpoint=None, withOrderLines=True, withClient=True, withAddress=True):
        return BlockingObservable()
    def resolve_readItems(self, _, readItemsInput, period, occurencesNumber, callbackEndpoint=None, withClients=True, withAddress=True, withPurchaseOrders=True):
        return BlockingObservable()
    pass
# schema
schema = Schema(query=Query, mutation=Mutation, subscription=Subscription)
pass
