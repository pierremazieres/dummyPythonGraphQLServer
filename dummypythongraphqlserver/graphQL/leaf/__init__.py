# coding=utf-8
# import
from graphene import ObjectType, ID
# output
class CommonField(ObjectType):
    id = ID(required=True)
    def resolve_id(self,_):
        return self.id
    pass
pass
