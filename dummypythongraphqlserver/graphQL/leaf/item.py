# coding=utf-8
# import
from graphene import Float, String
from dummypythongraphqlserver.graphQL.leaf import CommonField
from dummypythongraphqlserver.dataAccess.item import ItemDAO
# output
class LeafItemField(CommonField):
    name = String(required=True)
    price = Float(required=True)
    def resolve_name(self,_):
        return self.name
    def resolve_price(self,_):
        return self.price
    # shared resolver
    @staticmethod
    def resolveItems(ids=None, name=None, price=None, orderLinesIds=None, limit=None, offset=None):
        # read data
        readItems = ItemDAO.read(ids, name, price, orderLinesIds, limit, offset)
        # graphQL response
        itemsField = list()
        for item in readItems:
            itemField = LeafItemField(id=item.id, name=item.name, price=item.price)
            itemsField.append(itemField)
        return itemsField
    pass
pass
