# coding=utf-8
# import
from graphene import String
from dummypythongraphqlserver.graphQL.leaf import CommonField
from dummypythongraphqlserver.dataAccess.client import ClientDAO
# output
class LeafClientField(CommonField):
    firstName = String(required=True)
    lastName = String(required=True)
    def resolve_firstName(self,_):
        return self.firstName
    def resolve_lastName(self,_):
        return self.lastName
    # shared resolver
    @staticmethod
    def resolveClients(ids=None, firstName=None, lastName=None, limit=None, addressesIds=None, ordersIds=None, itemsIds=None, offset=None):
        # read data
        readClients = ClientDAO.read(ids, firstName, lastName, limit, addressesIds, ordersIds, itemsIds, offset)
        # graphQL response
        clientsField = list()
        for client in readClients:
            clientField = LeafClientField(id=client.id, firstName=client.firstName, lastName=client.lastName)
            clientsField.append(clientField)
        return clientsField
    pass
pass
