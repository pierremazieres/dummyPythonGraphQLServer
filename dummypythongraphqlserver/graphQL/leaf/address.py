# coding=utf-8
# import
from graphene import String
from dummypythongraphqlserver.graphQL.leaf import CommonField
from dummypythongraphqlserver.dataAccess.address import AddressDAO
# output
class LeafAddressField(CommonField):
    houseNumber = String(required=True)
    street = String(required=True)
    zipCode = String(required=True)
    city = String(required=True)
    def resolve_houseNumber(self,_):
        return self.houseNumber
    def resolve_street(self,_):
        return self.street
    def resolve_zipCode(self,_):
        return self.zipCode
    def resolve_city(self,_):
        return self.city
    # shared resolver
    @staticmethod
    def resolveAddresses(ids=None, houseNumber=None, street=None, zipCode=None, city=None, clientsIds=None, limit=None, offset=None):
        # read data
        readAddresses = AddressDAO.read(ids, houseNumber, street, zipCode, city, clientsIds, limit, offset)
        # graphQL response
        addressesField = list()
        for address in readAddresses:
            addressField = LeafAddressField(id=address.id, houseNumber=address.houseNumber, street=address.street, zipCode=address.zipCode, city=address.city)
            addressesField.append(addressField)
        return addressesField
    pass
pass
