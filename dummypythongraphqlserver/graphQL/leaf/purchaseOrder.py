# coding=utf-8
# import
from graphene import Int
from dummypythongraphqlserver.graphQL.leaf import CommonField
from dummypythongraphqlserver.dataAccess.purchaseOrder import PurchaseOrderDAO
# output
class LeafPurchaseOrderField(CommonField):
    creationDate = Int(required=True)
    def resolve_creationDate(self,_):
        return self.creationDate
    # shared resolver
    @staticmethod
    def resolvePurchaseOrders(ids=None, minimumCreationDate=None, maximumCreationDate=None, clientsIds=None, itemsIds=None, limit=None, offset=None):
        # read data
        readPurchaseOrders = PurchaseOrderDAO.read(ids, minimumCreationDate, maximumCreationDate, clientsIds, itemsIds, limit, offset)
        # graphQL response
        purchaseOrdersField = list()
        for purchaseOrder in readPurchaseOrders:
            purchaseOrderField = LeafPurchaseOrderField(id=purchaseOrder.id, creationDate=purchaseOrder.creationDate)
            purchaseOrdersField.append(purchaseOrderField)
        return purchaseOrdersField
    pass
pass
