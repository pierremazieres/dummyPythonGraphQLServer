# coding=utf-8
# import
from graphene import Int, String, Field, List, NonNull, Mutation, ID, InputObjectType, Boolean
from dummypythongraphqlserver.entities import Address
from dummypythongraphqlserver.graphQL.leaf.address import LeafAddressField
from dummypythongraphqlserver.graphQL.leaf.client import LeafClientField
from dummypythongraphqlserver.graphQL.node.purchaseOrder import NodePurchaseOrderField
from dummypythongraphqlserver.graphQL.root import ReadInput, DeleteInput, UpdateInput
from dummypythongraphqlserver.dataAccess.address import AddressDAO
# input
class DeleteAddressesInput(DeleteInput, InputObjectType):
    houseNumber = Int()
    street = String()
    zipCode = Int()
    city = String()
class ReadAddressesInput(ReadInput, DeleteAddressesInput):
    pass
class CreateAddressesInput(InputObjectType):
    houseNumber = Int(required=True)
    street = String(required=True)
    zipCode = Int(required=True)
    city = String(required=True)
class UpdateAddressesInput(UpdateInput, CreateAddressesInput):
    pass
# query
class RootAddressField(LeafAddressField):
    # inner client
    # INFO : this client is derivated from leaf (and not node) because it must not have address (to avoid circular references)
    class ClientAddressField(LeafClientField):
        purchaseOrders = Field(List(NonNull(NodePurchaseOrderField)), required=True)
        def resolve_purchaseOrders(self, _):
            return NodePurchaseOrderField.resolvePurchaseOrders([self.id])
    # address class
    clients = Field(List(NonNull(ClientAddressField)), required=True)
    def resolve_clients(self,_):
        leafClientFields = LeafClientField.resolveClients(addressesIds=[self.id])
        rootClientFields = list()
        for leafClientField in leafClientFields:
            rootClientField = RootAddressField.ClientAddressField(id=leafClientField.id, firstName=leafClientField.firstName, lastName=leafClientField.lastName)
            rootClientFields.append(rootClientField)
        return rootClientFields
    pass
# mutation
class DeleteAddresses(Mutation):
    class Arguments:
        deleteAddressesInput = DeleteAddressesInput(required=True)
    # INFO : at least 1 return field is needed by graphene framework
    confirmation = Boolean(required=True)
    def mutate(self, _, deleteAddressesInput):
        AddressDAO.delete(ids=deleteAddressesInput.ids, houseNumber=deleteAddressesInput.houseNumber, street=deleteAddressesInput.street, zipCode=deleteAddressesInput.zipCode, city=deleteAddressesInput.city)
        return DeleteAddresses(confirmation=True)
    pass
class CreateAddresses(Mutation):
    class Arguments:
        createAddressesInput = List(NonNull(CreateAddressesInput), required=True)
        withAddress = Boolean()
    readAddresses = Field(List(NonNull(LeafAddressField)), required=True)
    def mutate(self, _, createAddressesInput):
        # create each address
        entitiesAddressesInputs = list()
        for lineAddressesInput in createAddressesInput:
            entityAddress = Address(houseNumber=lineAddressesInput.houseNumber, street=lineAddressesInput.street, zipCode=lineAddressesInput.zipCode, city=lineAddressesInput.city)
            entitiesAddressesInputs.append(entityAddress)
            pass
        addressOutputs = AddressDAO.create(entitiesAddressesInputs)
        return CreateAddresses(readAddresses=addressOutputs)
    pass
class UpdateAddresses(Mutation):
    class Arguments:
        updateAddressesInput = List(NonNull(UpdateAddressesInput), required=True)
    confirmation = Boolean(required=True)
    def mutate(self, _, updateAddressesInput):
        # update each address
        entitiesAddressesInputs = list()
        for lineAddressesInput in updateAddressesInput:
            entityAddress = Address(id=lineAddressesInput.id, houseNumber=lineAddressesInput.houseNumber, street=lineAddressesInput.street, zipCode=lineAddressesInput.zipCode, city=lineAddressesInput.city)
            entitiesAddressesInputs.append(entityAddress)
            pass
        AddressDAO.update(entitiesAddressesInputs)
        return UpdateAddresses(confirmation=True)
    pass
pass
