# coding=utf-8
# import
from graphene import Field, List, NonNull, Int, Mutation, ID, InputObjectType, Boolean
from dummypythongraphqlserver.entities import PurchaseOrder, OrderLine
from dummypythongraphqlserver.graphQL.leaf.purchaseOrder import LeafPurchaseOrderField
from dummypythongraphqlserver.graphQL.leaf.client import LeafClientField
from dummypythongraphqlserver.graphQL.node.orderLine import NodeOrderLineField
from dummypythongraphqlserver.graphQL.node.client import NodeClientField
from dummypythongraphqlserver.graphQL.root import ReadInput, DeleteInput, UpdateInput
from dummypythongraphqlserver.dataAccess.purchaseOrder import PurchaseOrderDAO
from dummypythongraphqlserver.dataAccess.orderLine import OrderLineDAO
# input
class DeletePurchaseOrdersInput(DeleteInput, InputObjectType):
    minimumCreationDate = Int()
    maximumCreationDate = Int()
class ReadPurchaseOrdersInput(ReadInput, DeletePurchaseOrdersInput):
    pass
class CreateOrderLineInput(InputObjectType):
    quantity = Int(required=True)
    itemId = ID(required=True)
class UpdateOrderLineInput(UpdateInput, CreateOrderLineInput):
    pass
class WritePurchaseOrdersInput(InputObjectType):
    creationDate = Int(required=True)
    clientId = ID(required=True)
class CreatePurchaseOrdersInput(WritePurchaseOrdersInput):
    orderLines = List(NonNull(CreateOrderLineInput), required=True)
class UpdatePurchaseOrdersInput(UpdateInput, WritePurchaseOrdersInput):
    orderLines = List(NonNull(UpdateOrderLineInput), required=True)
# query
class RootPurchaseOrderField(LeafPurchaseOrderField):
    orderLines = Field(List(NonNull(NodeOrderLineField)), required=True)
    client = Field(NodeClientField, required=True)
    def resolve_orderLines(self,_):
        return NodeOrderLineField.resolveOrderLines([self.id])
    def resolve_client(self,_):
        clientsField = LeafClientField.resolveClients(ordersIds=[self.id])
        clientField = clientsField[0]
        return clientField
    pass
# mutation
class DeletePurchaseOrders(Mutation):
    class Arguments:
        deletePurchaseOrdersInput = DeletePurchaseOrdersInput(required=True)
    # INFO : at least 1 return field is needed by graphene framework
    confirmation = Boolean(required=True)
    def mutate(self, _, deletePurchaseOrdersInput):
        PurchaseOrderDAO.delete(ids=deletePurchaseOrdersInput.ids, minimumCreationDate=deletePurchaseOrdersInput.minimumCreationDate, maximumCreationDate=deletePurchaseOrdersInput.maximumCreationDate)
        return DeletePurchaseOrders(confirmation=True)
    pass
class CreatePurchaseOrders(Mutation):
    class Arguments:
        createPurchaseOrdersInput = List(NonNull(CreatePurchaseOrdersInput), required=True)
        withClient = Boolean()
        withAddress = Boolean()
        withOrderLines = Boolean()
    readPurchaseOrders = Field(List(NonNull(RootPurchaseOrderField)), required=True)
    '''INFO : keep all parameters, even the ones you do not use later
    they will be used by graphen framework
    i.e : withAddress, withClient(s), withPurchaseOrders, withOrderLines'''
    def mutate(self, _, createPurchaseOrdersInput, withClient=True, withAddress=True, withOrderLines=True):
        # create each purchase order (and associated client id)
        entitiesPurchaseOrdersInputs = dict()
        for linePurchaseOrdersInput in createPurchaseOrdersInput:
            entityPurchaseOrder = PurchaseOrder(creationDate=linePurchaseOrdersInput.creationDate)
            entitiesPurchaseOrdersInputs[entityPurchaseOrder] = linePurchaseOrdersInput.clientId
        purchaseOrderOutputs = PurchaseOrderDAO.create(entitiesPurchaseOrdersInputs)
        # create each order line (and associated order & item ids)
        entitiesOrderLineInputs = dict()
        for purchaseOrderIndex, linePurchaseOrdersInput in enumerate(createPurchaseOrdersInput):
            orderId = purchaseOrderOutputs[purchaseOrderIndex].id
            for orderLine in linePurchaseOrdersInput.orderLines:
                entityOrderLine = OrderLine(quantity=orderLine.quantity)
                entitiesOrderLineInputs[entityOrderLine] = [orderId, orderLine.itemId]
                pass
            pass
        OrderLineDAO.create(entitiesOrderLineInputs)
        return CreatePurchaseOrders(readPurchaseOrders=purchaseOrderOutputs)
    pass
class UpdatePurchaseOrders(Mutation):
    class Arguments:
        updatePurchaseOrdersInput = List(NonNull(UpdatePurchaseOrdersInput), required=True)
    confirmation = Boolean(required=True)
    def mutate(self, _, updatePurchaseOrdersInput):
        # update each purchase order (and associated client id)
        entitiesPurchaseOrdersInputs = dict()
        orderIds = list()
        for linePurchaseOrdersInput in updatePurchaseOrdersInput:
            entityPurchaseOrder = PurchaseOrder(id=linePurchaseOrdersInput.id, creationDate=linePurchaseOrdersInput.creationDate)
            entitiesPurchaseOrdersInputs[entityPurchaseOrder] = linePurchaseOrdersInput.clientId
            orderIds.append(linePurchaseOrdersInput.id)
            pass
        PurchaseOrderDAO.update(entitiesPurchaseOrdersInputs)
        # delete/insert each order line (and associated order & item ids)
        OrderLineDAO.delete(orderIds)
        entitiesOrderLineInputs = dict()
        for linePurchaseOrdersInput in updatePurchaseOrdersInput:
            orderId = linePurchaseOrdersInput.id
            for orderLine in linePurchaseOrdersInput.orderLines:
                entityOrderLine = OrderLine(quantity=orderLine.quantity)
                entitiesOrderLineInputs[entityOrderLine] = [orderId, orderLine.itemId]
                pass
            pass
        OrderLineDAO.create(entitiesOrderLineInputs)
        return UpdatePurchaseOrders(confirmation=True)
    pass
pass
