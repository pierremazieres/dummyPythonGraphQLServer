# coding=utf-8
# import
from graphene import Field, List, NonNull, String, Mutation, ID, InputObjectType, Boolean
from dummypythongraphqlserver.entities import Client
from dummypythongraphqlserver.graphQL.node.client import NodeClientField
from dummypythongraphqlserver.graphQL.node.purchaseOrder import NodePurchaseOrderField
from dummypythongraphqlserver.graphQL.root import ReadInput, DeleteInput, UpdateInput
from dummypythongraphqlserver.dataAccess.client import ClientDAO
# input
class DeleteClientsInput(DeleteInput, InputObjectType):
    firstName = String()
    lastName = String()
class ReadClientsInput(ReadInput, DeleteClientsInput):
    pass
class CreateClientsInput(InputObjectType):
    firstName = String(required=True)
    lastName = String(required=True)
    addressId = ID(required=True)
class UpdateClientsInput(UpdateInput, CreateClientsInput):
    pass
# query
class RootClientField(NodeClientField):
    purchaseOrders = Field(List(NonNull(NodePurchaseOrderField)), required=True)
    def resolve_purchaseOrders(self,_):
        return NodePurchaseOrderField.resolvePurchaseOrders([self.id])
    pass
# mutation
class DeleteClients(Mutation):
    class Arguments:
        deleteClientsInput = DeleteClientsInput(required=True)
    # INFO : at least 1 return field is needed by graphene framework
    confirmation = Boolean(required=True)
    def mutate(self, _, deleteClientsInput):
        ClientDAO.delete(ids=deleteClientsInput.ids, firstName=deleteClientsInput.firstName, lastName=deleteClientsInput.lastName)
        return DeleteClients(confirmation=True)
    pass
class CreateClients(Mutation):
    class Arguments:
        createClientsInput = List(NonNull(CreateClientsInput), required=True)
        withAddress = Boolean()
    readClients = Field(List(NonNull(NodeClientField)), required=True)
    '''INFO : keep all parameters, even the ones you do not use later
    they will be used by graphen framework
    i.e : withAddress, withClient(s), withPurchaseOrders, withOrderLines'''
    def mutate(self, _, createClientsInput, withAddress=True):
        # create each client (and associated address id)
        entitiesClientsInputs = dict()
        for lineClientsInput in createClientsInput:
            entityClient = Client(firstName=lineClientsInput.firstName, lastName=lineClientsInput.lastName)
            entitiesClientsInputs[entityClient] = lineClientsInput.addressId
            pass
        clientOutputs = ClientDAO.create(entitiesClientsInputs)
        return CreateClients(readClients=clientOutputs)
    pass
class UpdateClients(Mutation):
    class Arguments:
        updateClientsInput = List(NonNull(UpdateClientsInput), required=True)
    confirmation = Boolean(required=True)
    def mutate(self, _, updateClientsInput):
        # update each client (and associated address id)
        entitiesClientsInputs = dict()
        for lineClientsInput in updateClientsInput:
            entityClient = Client(id=lineClientsInput.id, firstName=lineClientsInput.firstName, lastName=lineClientsInput.lastName)
            entitiesClientsInputs[entityClient] = lineClientsInput.addressId
            pass
        ClientDAO.update(entitiesClientsInputs)
        return UpdateClients(confirmation=True)
    pass
pass
