# coding=utf-8
# import
from graphene import ID, Int, List
# input
class DeleteInput():
    ids = List(ID)
class ReadInput(DeleteInput):
    limit = Int()
    offset = Int()
class UpdateInput():
    id = ID(required=True)
pass
