# coding=utf-8
# import
from graphene import Field, Int, List, NonNull, String, Mutation, ID, InputObjectType, Boolean
from dummypythongraphqlserver.entities import Item
from dummypythongraphqlserver.graphQL.leaf.item import LeafItemField
from dummypythongraphqlserver.graphQL.leaf.purchaseOrder import LeafPurchaseOrderField
from dummypythongraphqlserver.graphQL.leaf.client import LeafClientField
from dummypythongraphqlserver.graphQL.node.client import NodeClientField
from dummypythongraphqlserver.graphQL.root import ReadInput, DeleteInput, UpdateInput
from dummypythongraphqlserver.dataAccess.item import ItemDAO
from dummypythongraphqlserver.dataAccess.orderLine import OrderLineDAO
# input
class DeleteItemsInput(DeleteInput, InputObjectType):
    name = String()
    price = String()
class ReadItemsInput(ReadInput, DeleteItemsInput):
    pass
class CreateItemsInput(InputObjectType):
    name = String(required=True)
    price = String(required=True)
class UpdateItemsInput(UpdateInput, CreateItemsInput):
    pass
# query
class RootItemField(LeafItemField):
    # inner client
    class ClientItemField(NodeClientField):
        # inner purchase order
        # INFO : this purchase order is derivated from leaf (and not node) because it must not have item (to avoid circular references)
        class PurchaseOrderClientField(LeafPurchaseOrderField):
            quantity = Int(required=True)
            def resolve_quantity(self, _):
                # read data
                orderLines = OrderLineDAO.read(ordersIds=[self.id])
                # graphQL response
                orderLine = orderLines[0]
                return orderLine.quantity
        purchaseOrders = Field(List(NonNull(PurchaseOrderClientField)), required=True)
        def resolve_purchaseOrders(self, _):
            leafPurchaseOrderFields = LeafPurchaseOrderField.resolvePurchaseOrders(clientsIds=[self.id], itemsIds=[self.itemId])
            rootPurchaseOrderFields = list()
            for leafPurchaseOrderField in leafPurchaseOrderFields:
                rootPurchaseOrderField = RootItemField.ClientItemField.PurchaseOrderClientField(id=leafPurchaseOrderField.id, creationDate=leafPurchaseOrderField.creationDate)
                rootPurchaseOrderFields.append(rootPurchaseOrderField)
            return rootPurchaseOrderFields
    # item class
    clients = Field(List(NonNull(ClientItemField)), required=True)
    def resolve_clients(self,_):
        leafClientFields = LeafClientField.resolveClients(itemsIds=[self.id])
        rootClientFields = list()
        for leafClientField in leafClientFields:
            rootClientField = RootItemField.ClientItemField(id=leafClientField.id, firstName=leafClientField.firstName, lastName=leafClientField.lastName)
            # INFO : we save item id to select only related purchase orders
            rootClientField.itemId=self.id
            rootClientFields.append(rootClientField)
        return rootClientFields
    pass
# mutation
class DeleteItems(Mutation):
    class Arguments:
        deleteItemsInput = DeleteItemsInput(required=True)
    # INFO : at least 1 return field is needed by graphene framework
    confirmation = Boolean(required=True)
    def mutate(self, _, deleteItemsInput):
        ItemDAO.delete(ids=deleteItemsInput.ids, name=deleteItemsInput.name, price=deleteItemsInput.price)
        return DeleteItems(confirmation=True)
    pass
class CreateItems(Mutation):
    class Arguments:
        createItemsInput = List(NonNull(CreateItemsInput), required=True)
    readItems = Field(List(NonNull(LeafItemField)), required=True)
    def mutate(self, _, createItemsInput):
        # create each item
        entitiesItemsInputs = list()
        for lineItemsInput in createItemsInput:
            entityItem = Item(name=lineItemsInput.name, price=lineItemsInput.price)
            entitiesItemsInputs.append(entityItem)
            pass
        itemOutputs = ItemDAO.create(entitiesItemsInputs)
        return CreateItems(readItems=itemOutputs)
    pass
class UpdateItems(Mutation):
    class Arguments:
        updateItemsInput = List(NonNull(UpdateItemsInput), required=True)
    confirmation = Boolean(required=True)
    def mutate(self, _, updateItemsInput):
        # update each item
        entitiesItemsInputs = list()
        for lineItemsInput in updateItemsInput:
            entityItem = Item(id=lineItemsInput.id, name=lineItemsInput.name, price=lineItemsInput.price)
            entitiesItemsInputs.append(entityItem)
            pass
        ItemDAO.update(entitiesItemsInputs)
        return UpdateItems(confirmation=True)
    pass
pass
