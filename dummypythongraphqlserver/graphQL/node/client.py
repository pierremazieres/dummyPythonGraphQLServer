# coding=utf-8
# import
from graphene import Field
from dummypythongraphqlserver.graphQL.leaf.client import LeafClientField
from dummypythongraphqlserver.graphQL.leaf.address import LeafAddressField
# output
class NodeClientField(LeafClientField):
    address = Field(LeafAddressField, required=True)
    def resolve_address(self,_):
        addressesField = LeafAddressField.resolveAddresses(clientsIds=[self.id])
        addressField = addressesField[0]
        return addressField
    pass
pass
