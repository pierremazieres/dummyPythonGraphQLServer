# coding=utf-8
# import
from graphene import Field, Int
from dummypythongraphqlserver.graphQL.leaf import CommonField
from dummypythongraphqlserver.dataAccess.orderLine import OrderLineDAO
from dummypythongraphqlserver.graphQL.leaf.item import LeafItemField
# output
class NodeOrderLineField(CommonField):
    item = Field(LeafItemField,required=True)
    quantity = Int(required=True)
    def resolve_item(self,_):
        itemsField = LeafItemField.resolveItems(orderLinesIds=[self.id])
        itemField = itemsField[0]
        return itemField
    def resolve_quantity(self,_):
        return self.quantity
    # shared resolver
    @staticmethod
    def resolveOrderLines(ordersIds):
        # read data
        orderLines = OrderLineDAO.read(ordersIds=ordersIds)
        # graphQL response
        orderLinesField = list()
        for orderLine in orderLines:
            orderLineField = NodeOrderLineField(id=orderLine.id, quantity=orderLine.quantity)
            orderLinesField.append(orderLineField)
        return orderLinesField
    pass
pass
