# coding=utf-8
# import
from graphene import Field, List, NonNull
from dummypythongraphqlserver.graphQL.leaf.purchaseOrder import LeafPurchaseOrderField
from dummypythongraphqlserver.graphQL.node.orderLine import NodeOrderLineField
# output
class NodePurchaseOrderField(LeafPurchaseOrderField):
    orderLines = Field(List(NonNull(NodeOrderLineField)), required=True)
    def resolve_orderLines(self,_):
        return NodeOrderLineField.resolveOrderLines([self.id])
    # shared resolver
    @staticmethod
    def resolvePurchaseOrders(clientsIds):
        leafPurchaseOrderFields = LeafPurchaseOrderField.resolvePurchaseOrders(clientsIds=clientsIds)
        rootPurchaseOrderFields = list()
        for leafPurchaseOrderField in leafPurchaseOrderFields:
            rootPurchaseOrderField = NodePurchaseOrderField(id=leafPurchaseOrderField.id, creationDate=leafPurchaseOrderField.creationDate)
            rootPurchaseOrderFields.append(rootPurchaseOrderField)
        return rootPurchaseOrderFields
    pass
pass
