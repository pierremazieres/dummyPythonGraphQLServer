#!/usr/bin/env python3
# coding=utf-8
# entities
''' INFO :
in order to avoid circular references, there is absolutly no link between entities
i.e : a client does not reference an address (& an address does not reference clients)
'''
class Entity():
    def __init__(self,id):
        self.id = id
        pass
    def __eq__(self,other):
        return self.id == other.id
    def __hash__(self):
        return hash(self.id)
    pass
class Item(Entity):
    def __init__(self,name,price,id=None):
        super().__init__(id)
        self.name = name
        self.price = price
        pass
    def __eq__(self,other):
        return super().__eq__(other) \
            and self.name == other.name \
            and self.price == other.price
    def __hash__(self):
        return hash(((super().__hash__(), self.name, self.price)))
    pass
class Address(Entity):
    def __init__(self,houseNumber,street,zipCode,city,id=None):
        super().__init__(id)
        self.houseNumber = houseNumber
        self.street = street
        self.zipCode = zipCode
        self.city = city
        pass
    def __eq__(self,other):
        return super().__eq__(other) \
            and self.houseNumber == other.houseNumber \
            and self.street == other.street \
            and self.zipCode == other.zipCode \
            and self.city == other.city
        pass
    def __hash__(self):
        return hash(((super().__hash__(), self.houseNumber, self.street, self.zipCode, self.city)))
    pass
class Client(Entity):
    def __init__(self,firstName,lastName,id=None):
        super().__init__(id)
        self.firstName = firstName
        self.lastName = lastName
        pass
    def __eq__(self,other):
        return super().__eq__(other) \
            and self.firstName == other.firstName \
            and self.lastName == other.lastName
    def __hash__(self):
        return hash(((self.id, self.firstName, self.lastName)))
    pass
class PurchaseOrder(Entity):
    def __init__(self,creationDate, id=None):
        super().__init__(id)
        self.creationDate = creationDate
        pass
    def __eq__(self,other):
        return super().__eq__(other) \
            and self.creationDate == other.creationDate
    def __hash__(self):
        return hash(((super().__hash__(), self.creationDate)))
    pass
class OrderLine(Entity):
    def __init__(self,quantity,id=None):
        super().__init__(id)
        self.quantity = quantity
        pass
    def __eq__(self,other):
        return super().__eq__(other) \
            and self.quantity == other.quantity
    def __hash__(self):
        return hash(((super().__hash__(), self.quantity)))
    pass
pass