# coding=utf-8
# import
from sqlite3 import connect
from dummypythongraphqlserver.dataAccess import DB
from dummypythongraphqlserver.entities import Item
# data access object
class ItemDAO():
    @staticmethod
    def create(items):
        statement = "INSERT INTO ITEM (NAME,PRICE) VALUES "
        parameters = list()
        # fill each item
        for item in items :
            statement += "(?,?),"
            parameters += [item.name,item.price]
            pass
        statement = statement[:-1]
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        # INFO : we can update input clients in dictionary keys
        newItems = list()
        try:
            # insert data
            cursor.execute(statement,parameters)
            connection.commit()
            # retrieve IDs
            for originalItem in items:
                newItem = ItemDAO.read(name=originalItem.name, price=originalItem.price)[0]
                newItems.append(newItem)
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return newItems
    @staticmethod
    def read(ids=None,name=None,price=None, orderLinesIds=None, limit=None,offset=None):
        statement = "SELECT I.NAME, I.PRICE, I.ID FROM ITEM I"
        parameters = list()
        # joins
        if orderLinesIds:
            statement += " JOIN ORDER_LINE OL ON OL.ITEM_ID = I.ID"
        # where
        statement += " WHERE 0=0"
        if ids:
            statement += " AND I.ID IN ("+','.join(['?']*len(ids))+")"
            parameters += ids
        if name:
            statement += " AND UPPER(I.NAME) LIKE UPPER(?)"
            parameters.append('%'+str(name)+'%')
        if price:
            statement += " AND I.PRICE LIKE ?"
            parameters.append('%'+str(price)+'%')
        if orderLinesIds:
            statement += " AND OL.ID IN ("+','.join(['?']*len(orderLinesIds))+")"
            parameters += orderLinesIds
        # order
        statement += " ORDER BY I.ID ASC"
        # shift
        if limit:
            statement += " LIMIT ?"
            parameters.append(limit)
        if offset:
            statement += " OFFSET ?"
            parameters.append(offset)
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        items = list()
        try:
            results = cursor.execute(statement,parameters)
            for result in results:
                item = Item(*result)
                items.append(item)
                pass
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return items
    @staticmethod
    def update(items):
        statement = "WITH TMP(ID,NAME,PRICE) AS (VALUES"
        parameters = list()
        for item in items :
            statement += "(?,?,?),"
            parameters += [item.id,item.name,item.price]
        statement = statement[:-1]
        statement += """)
UPDATE ITEM
SET NAME = (SELECT NAME FROM TMP WHERE ITEM.ID=TMP.ID),
PRICE = (SELECT PRICE FROM TMP WHERE ITEM.ID=TMP.ID)
WHERE ID IN (SELECT ID FROM TMP)"""
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        try:
            cursor.execute(statement,parameters)
            connection.commit()
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        pass
    @staticmethod
    def delete(ids=None,name=None,price=None):
        statement = "DELETE FROM ITEM WHERE 0=0"
        parameters = list()
        if ids:
            statement += " AND ID IN ("+','.join(['?']*len(ids))+")"
            parameters += ids
        if name:
            statement += " AND UPPER(NAME) LIKE UPPER(?)"
            parameters.append('%'+str(name)+'%')
        if price:
            statement += " AND PRICE LIKE ?"
            parameters.append('%'+str(price)+'%')
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        try:
            cursor.execute(statement,parameters)
            connection.commit()
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
    pass
pass

