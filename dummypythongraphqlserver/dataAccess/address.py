# coding=utf-8
# import
from sqlite3 import connect
from dummypythongraphqlserver.dataAccess import DB
from dummypythongraphqlserver.entities import Address
# data access object
class AddressDAO():
    @staticmethod
    def create(addresses):
        statement = "INSERT INTO ADDRESS (HOUSE_NUMBER,STREET,ZIPCODE,CITY) VALUES "
        parameters = list()
        # fill each address
        for address in addresses :
            statement += "(?,?,?,?),"
            parameters += [address.houseNumber,address.street,address.zipCode,address.city]
            pass
        statement = statement[:-1]
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        # INFO : we can update input addresses in dictionary keys
        newAddresses = list()
        try:
            # insert data
            cursor.execute(statement,parameters)
            connection.commit()
            # retrieve IDs
            for originalAddress in addresses:
                newAddress = AddressDAO.read(houseNumber=originalAddress.houseNumber, street=originalAddress.street, zipCode=originalAddress.zipCode, city=originalAddress.city)[0]
                newAddresses.append(newAddress)
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return newAddresses
    @staticmethod
    def read(ids=None, houseNumber=None, street=None, zipCode=None, city=None, clientsIds=None, limit=None, offset=None):
        statement = "SELECT A.HOUSE_NUMBER, A.STREET, A.ZIPCODE, A.CITY, A.ID FROM ADDRESS A"
        parameters = list()
        # joins
        if clientsIds:
            statement += " JOIN CLIENT C ON C.ADDRESS_ID = A.ID"
        # where
        statement += " WHERE 0=0"
        if ids:
            statement += " AND A.ID IN ("+','.join(['?']*len(ids))+")"
            parameters += ids
        if houseNumber:
            statement += " AND A.HOUSE_NUMBER LIKE ?"
            parameters.append('%'+str(houseNumber)+'%')
        if street:
            statement += " AND UPPER(A.STREET) LIKE UPPER(?)"
            parameters.append('%'+str(street)+'%')
        if zipCode:
            statement += " AND A.ZIPCODE LIKE ?"
            parameters.append('%'+str(zipCode)+'%')
        if city:
            statement += " AND UPPER(A.CITY) LIKE UPPER(?)"
            parameters.append('%'+str(city)+'%')
        if clientsIds:
            statement += " AND C.ID IN ("+','.join(['?']*len(clientsIds))+")"
            parameters += clientsIds
        # order
        statement += " ORDER BY A.ID ASC"
        # shift
        if limit:
            statement += " LIMIT ?"
            parameters.append(limit)
        if offset:
            statement += " OFFSET ?"
            parameters.append(offset)
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        addresses = list()
        try:
            results = cursor.execute(statement,parameters)
            for result in results:
                address = Address(*result)
                addresses.append(address)
                pass
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return addresses
    @staticmethod
    def update(addresses):
        statement = "WITH TMP(ID,HOUSE_NUMBER,STREET,ZIPCODE,CITY) AS (VALUES"
        parameters = list()
        # fill each address
        for address in addresses :
            statement += "(?,?,?,?,?),"
            parameters += [address.id,address.houseNumber,address.street,address.zipCode,address.city]
        statement = statement[:-1]
        statement += """)
UPDATE ADDRESS
SET HOUSE_NUMBER = (SELECT HOUSE_NUMBER FROM TMP WHERE ADDRESS.ID=TMP.ID),
STREET = (SELECT STREET FROM TMP WHERE ADDRESS.ID=TMP.ID),
ZIPCODE = (SELECT ZIPCODE FROM TMP WHERE ADDRESS.ID=TMP.ID),
CITY = (SELECT CITY FROM TMP WHERE ADDRESS.ID=TMP.ID)
WHERE ID IN (SELECT ID FROM TMP)"""
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        try:
            cursor.execute(statement,parameters)
            connection.commit()
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        pass
    @staticmethod
    def delete(ids=None,houseNumber=None,street=None,zipCode=None,city=None):
        statement = "DELETE FROM ADDRESS WHERE 0=0"
        parameters = list()
        if ids:
            statement += " AND ID IN ("+','.join(['?']*len(ids))+")"
            parameters += ids
        if houseNumber:
            statement += " AND HOUSE_NUMBER LIKE ?"
            parameters.append('%'+str(houseNumber)+'%')
        if street:
            statement += " AND UPPER(STREET) LIKE UPPER(?)"
            parameters.append('%'+str(street)+'%')
        if zipCode:
            statement += " AND ZIPCODE LIKE ?"
            parameters.append('%'+str(zipCode)+'%')
        if city:
            statement += " AND UPPER(CITY) LIKE UPPER(?)"
            parameters.append('%'+str(city)+'%')
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        try:
            cursor.execute(statement,parameters)
            connection.commit()
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
    pass
pass

