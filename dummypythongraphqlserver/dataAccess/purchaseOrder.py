# coding=utf-8
# import
from sqlite3 import connect
from dummypythongraphqlserver.dataAccess import DB
from dummypythongraphqlserver.entities import PurchaseOrder
from dummypythongraphqlserver.dataAccess.orderLine import OrderLineDAO
# data access object
class PurchaseOrderDAO():
    @staticmethod
    def create(purchaseOrders):
        statement = "INSERT INTO PURCHASE_ORDER (CREATION_DATE, CLIENT_ID) VALUES "
        parameters = list()
        # fill each purchase order (and associated client id)
        for purchaseOrder, clientId in purchaseOrders.items() :
            statement += "(?,?),"
            parameters += [purchaseOrder.creationDate,clientId]
            pass
        statement = statement[:-1]
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        # INFO : we can update input clients in dictionary keys
        newPurchaseOrders = list()
        try:
            # insert data
            cursor.execute(statement,parameters)
            connection.commit()
            # retrieve IDs
            for originalPurchaseOrder, clientId in purchaseOrders.items():
                newPurchaseOrder = PurchaseOrderDAO.read(minimumCreationDate=originalPurchaseOrder.creationDate, maximumCreationDate=originalPurchaseOrder.creationDate, clientsIds=[clientId])[0]
                newPurchaseOrders.append(newPurchaseOrder)
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return newPurchaseOrders
    @staticmethod
    def read(ids=None, minimumCreationDate=None, maximumCreationDate=None, clientsIds=None, itemsIds=None, limit=None, offset=None):
        statement = "SELECT PO.CREATION_DATE, PO.ID FROM PURCHASE_ORDER PO"
        parameters = list()
        # joins
        if clientsIds:
            statement += " JOIN CLIENT C ON C.ID = PO.CLIENT_ID"
        if itemsIds:
            statement += " JOIN ORDER_LINE OL ON OL.ORDER_ID = PO.ID"
        # where
        statement += " WHERE 0=0"
        if ids:
            statement += " AND PO.ID IN ("+','.join(['?']*len(ids))+")"
            parameters += ids
        if minimumCreationDate:
            statement += " AND PO.CREATION_DATE >= ?"
            parameters.append(minimumCreationDate)
        if maximumCreationDate:
            statement += " AND PO.CREATION_DATE <= ?"
            parameters.append(maximumCreationDate)
        if clientsIds:
            statement += " AND C.ID IN ("+','.join(['?']*len(clientsIds))+")"
            parameters += clientsIds
        if itemsIds:
            statement += " AND OL.ITEM_ID IN ("+','.join(['?']*len(itemsIds))+")"
            parameters += itemsIds
        # order
        statement += " ORDER BY PO.ID ASC"
        # shift
        if limit:
            statement += " LIMIT ?"
            parameters.append(limit)
        if offset:
            statement += " OFFSET ?"
            parameters.append(offset)
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        purchaseOrders = list()
        try:
            results = cursor.execute(statement,parameters)
            for result in results:
                purchaseOrder = PurchaseOrder(*result)
                purchaseOrders.append(purchaseOrder)
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return purchaseOrders
    @staticmethod
    def update(purchaseOrders):
        statement = "WITH TMP(ID, CREATION_DATE, CLIENT_ID) AS (VALUES"
        parameters = list()
        # fill each purchase order (and associated client id)
        for purchaseOrder, clientId in purchaseOrders.items():
            statement += "(?,?,?),"
            parameters += [purchaseOrder.id, purchaseOrder.creationDate, clientId]
        statement = statement[:-1]
        statement += """)
UPDATE PURCHASE_ORDER
SET CREATION_DATE = (SELECT CREATION_DATE FROM TMP WHERE PURCHASE_ORDER.ID=TMP.ID),
CLIENT_ID = (SELECT CLIENT_ID FROM TMP WHERE PURCHASE_ORDER.ID=TMP.ID)
WHERE ID IN (SELECT ID FROM TMP)"""
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        try:
            cursor.execute(statement,parameters)
            connection.commit()
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        pass
    @staticmethod
    def delete(ids=None,minimumCreationDate=None,maximumCreationDate=None):
        statement = "DELETE FROM PURCHASE_ORDER WHERE 0=0"
        parameters = list()
        if ids:
            statement += " AND ID IN ("+','.join(['?']*len(ids))+")"
            parameters += ids
        if minimumCreationDate:
            statement += " AND CREATION_DATE >= ?"
            parameters.append(minimumCreationDate)
        if maximumCreationDate:
            statement += " AND CREATION_DATE <= ?"
            parameters.append(maximumCreationDate)
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        purchaseOrders = PurchaseOrderDAO.read(ids, minimumCreationDate, maximumCreationDate)
        if purchaseOrders:
            orderIds = list()
            for purchaseOrder in purchaseOrders:
                orderIds.append(purchaseOrder.id)
        try:
            # order lines
            if purchaseOrders:
                OrderLineDAO.delete(orderIds)
            # purchase orders
            cursor.execute(statement,parameters)
            connection.commit()
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
    pass
pass

