# coding=utf-8
# import
from sqlite3 import connect
from dummypythongraphqlserver.dataAccess import DB
from dummypythongraphqlserver.entities import OrderLine
# data access object
class OrderLineDAO():
    @staticmethod
    def create(orderLines):
        statement = "INSERT INTO ORDER_LINE (QUANTITY,ORDER_ID,ITEM_ID) VALUES "
        parameters = list()
        # fill each order line (and associated order & item ids)
        for orderLine, externalIds in orderLines.items() :
            orderId = externalIds[0]
            itemId = externalIds[1]
            statement += "(?,?,?),"
            parameters += [orderLine.quantity,orderId,itemId]
            pass
        statement = statement[:-1]
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        # INFO : we can update input clients in dictionary keys
        newOrderLines = list()
        try:
            # insert data
            cursor.execute(statement,parameters)
            connection.commit()
            # retrieve IDs
            for originalOrderLine, externalIds in orderLines.items():
                orderId = externalIds[0]
                itemId = externalIds[1]
                newOrderLine = OrderLineDAO.read(quantity=originalOrderLine.quantity, ordersIds=[orderId], itemsId=[itemId])[0]
                newOrderLines.append(newOrderLine)
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return newOrderLines
    @staticmethod
    def read(ordersIds, itemsId=None, quantity=None):
        statement = "SELECT OL.QUANTITY, OL.ID FROM ORDER_LINE OL JOIN PURCHASE_ORDER PO ON PO.ID = OL.ORDER_ID WHERE OL.ORDER_ID IN ("+','.join(['?']*len(ordersIds))+")"
        parameters = ordersIds
        # where
        if itemsId:
            statement += " AND OL.ITEM_ID IN ("+','.join(['?']*len(itemsId))+")"
            parameters += itemsId
        if quantity:
            statement += " AND OL.QUANTITY LIKE ?"
            parameters.append(quantity)
        # order
        statement += " ORDER BY OL.ID ASC"
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        try:
            results = cursor.execute(statement,parameters)
            orderLines = list()
            for result in results:
                orderLine = OrderLine(*result)
                orderLines.append(orderLine)
                pass
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return orderLines
    @staticmethod
    def delete(orderIds):
        statement = "DELETE FROM ORDER_LINE WHERE ORDER_ID IN ("+','.join(['?']*len(orderIds))+")"
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        try:
            cursor.execute(statement,orderIds)
            connection.commit()
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
    pass
pass

