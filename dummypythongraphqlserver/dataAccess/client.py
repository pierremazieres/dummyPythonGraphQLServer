# coding=utf-8
# import
from sqlite3 import connect
from dummypythongraphqlserver.dataAccess import DB
from dummypythongraphqlserver.entities import Client
# data access object
class ClientDAO():
    @staticmethod
    def create(clients):
        statement = "INSERT INTO CLIENT (FIRST_NAME,LAST_NAME,ADDRESS_ID) VALUES "
        parameters = list()
        # fill each client (and associated address id)
        for client, addressId in clients.items() :
            statement += "(?,?,?),"
            parameters += [client.firstName, client.lastName, addressId]
            pass
        statement = statement[:-1]
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        # INFO : we can update input clients in dictionary keys
        newClients = list()
        try:
            # insert data
            cursor.execute(statement,parameters)
            connection.commit()
            # retrieve IDs
            for originalClient, addressId in clients.items():
                newClient = ClientDAO.read(firstName=originalClient.firstName, lastName=originalClient.lastName, addressesIds=[addressId])[0]
                newClients.append(newClient)
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return newClients
    @staticmethod
    def read(ids=None, firstName=None, lastName=None, limit=None, addressesIds=None, ordersIds=None, itemsIds=None, offset=None):
        statement = "SELECT C.FIRST_NAME, C.LAST_NAME, C.ID FROM CLIENT C"
        parameters = list()
        # joins
        if addressesIds:
            statement += " JOIN ADDRESS A ON A.ID = C.ADDRESS_ID"
        if ordersIds or itemsIds:
            statement += " JOIN PURCHASE_ORDER PO ON PO.CLIENT_ID = C.ID"
        if itemsIds:
            statement += " JOIN ORDER_LINE OL ON OL.ORDER_ID = PO.ID JOIN ITEM I ON I.ID = OL.ITEM_ID"
        # where
        statement += " WHERE 0=0"
        if ids:
            statement += " AND C.ID IN ("+','.join(['?']*len(ids))+")"
            parameters += ids
        if firstName:
            statement += " AND UPPER(C.FIRST_NAME) LIKE UPPER(?)"
            parameters.append('%'+str(firstName)+'%')
        if lastName:
            statement += " AND UPPER(C.LAST_NAME) LIKE UPPER(?)"
            parameters.append('%'+str(lastName)+'%')
        if addressesIds:
            statement += " AND A.ID IN ("+','.join(['?']*len(addressesIds))+")"
            parameters += addressesIds
        if ordersIds:
            statement += " AND PO.ID IN ("+','.join(['?']*len(ordersIds))+")"
            parameters += ordersIds
        if itemsIds:
            statement += " AND I.ID IN ("+','.join(['?']*len(itemsIds))+")"
            parameters += itemsIds
        # order
        statement += " ORDER BY C.ID ASC"
        # shift
        if limit:
            statement += " LIMIT ?"
            parameters.append(limit)
        if offset:
            statement += " OFFSET ?"
            parameters.append(offset)
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        clients = list()
        try:
            results = cursor.execute(statement,parameters)
            for result in results:
                client = Client(*result)
                clients.append(client)
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        return clients
    @staticmethod
    def update(clients):
        statement = "WITH TMP(ID,FIRST_NAME,LAST_NAME,ADDRESS_ID) AS (VALUES"
        parameters = list()
        # fill each client (and associated address id)
        for client, addressId in clients.items() :
            statement += "(?,?,?,?),"
            parameters += [client.id,client.firstName, client.lastName, addressId]
        statement = statement[:-1]
        statement += """)
UPDATE CLIENT
SET FIRST_NAME = (SELECT FIRST_NAME FROM TMP WHERE CLIENT.ID=TMP.ID),
LAST_NAME = (SELECT LAST_NAME FROM TMP WHERE CLIENT.ID=TMP.ID),
ADDRESS_ID = (SELECT ADDRESS_ID FROM TMP WHERE CLIENT.ID=TMP.ID)
WHERE ID IN (SELECT ID FROM TMP)"""
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        try:
            cursor.execute(statement,parameters)
            connection.commit()
            pass
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
        pass
    @staticmethod
    def delete(ids=None,firstName=None,lastName=None):
        statement = "DELETE FROM CLIENT WHERE 0=0"
        parameters = list()
        if ids:
            statement += " AND ID IN ("+','.join(['?']*len(ids))+")"
            parameters += ids
        if firstName:
            statement += " AND UPPER(FIRST_NAME) LIKE UPPER(?)"
            parameters.append('%'+str(firstName)+'%')
        if lastName:
            statement += " AND UPPER(LAST_NAME) LIKE UPPER(?)"
            parameters.append('%'+str(lastName)+'%')
        connection = connect(DB)
        cursor = connection.cursor()
        raisedExeption = None
        try:
            cursor.execute(statement,parameters)
            connection.commit()
        except Exception as exeception:
            raisedExeption = exeception
        finally:
            cursor.close()
            connection.close()
            if raisedExeption : raise raisedExeption
    pass
pass

